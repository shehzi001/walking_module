/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface_test.cpp
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */

#include <iostream>
#include <ros/ros.h>
#include <robot_controller_interface.h>
#include "oru_walk.h"

int main(int argc, char** argv) {

    ros::init(argc, argv, "robot_controller_interface");

    ros::NodeHandle nh_;
    sleep(1.0);

    oru_walk zeno_walk("walking_module", nh_);

    //zeno_walk.walk();

    zeno_walk.printMessage("Walking module is initilized");
    zeno_walk.walkControl();

    /*
    RobotControllerInterface robot_controller_interface(nh_);

    std::vector<std::string> names(26);
    std::vector<double> values;
    names = {"neck_roll", "neck_pitch", "neck_yaw", "waist",
            "left_shoulder_pitch", "left_shoulder_roll",
            "left_elbow_roll", "left_elbow_yaw",
            "right_shoulder_pitch", "right_shoulder_roll",
            "right_elbow_roll", "right_elbow_yaw",
            "left_hip_roll", "left_hip_yaw", "left_hip_pitch",
            "left_knee_pitch", "left_ankle_pitch", "left_ankle_roll",
            "right_hip_roll", "right_hip_yaw", "right_hip_pitch",
            "right_knee_pitch", "right_ankle_pitch", "right_ankle_roll",
            "right_wrist_yaw", "left_wrist_yaw"};
    values.resize(names.size());

    ROS_INFO("robot_controller_interface is running now.");


    ros::Rate loop_rate(10);

    while(ros::ok()) {
        ros::spinOnce();
        std::vector<double> positions;

        robot_controller_interface.readJointPositions(names, positions);

        if (positions.size() == names.size()) {
            for(int i=0;i<positions.size();i++) {
                std::cout<< names.at(i) << ": " << positions.at(i) << ", ";
            }
            std::cout<< "\n===========================" << std::endl;
        }

        robot_controller_interface.sendPositionCommand(names, values);
        
        loop_rate.sleep();
    }*/

    return 0;
}
