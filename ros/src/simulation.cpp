/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */


#include "oru_walk.h"


void oru_walk::initialize_simulation(WMG &wmg)
{
    // ----------------------
    screenWidth = wp.screenWidth;
    screenHeight = wp.screenHeight;

    // initial view
    zoom = wp.zoom;
    rotx = wp.rotx;
    roty = wp.roty;
    rotz = wp.rotz;
    transx = wp.transx;
    transy = wp.transy;

    wmg.getFootsteps(x_coord, y_coord, angle_rot);
	initSDL();
}

void oru_walk::simulate()
{
	if (zeno.support_foot == IGM_SUPPORT_RIGHT)
        {
            drawSDL(0, x_coord, y_coord, angle_rot, zeno.support_foot, zeno.state_model.q, zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, zeno.support_foot, zeno.state_model.q, zeno.left_foot_posture);
        }

}