/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface.cpp
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */

#include <robot_controller_interface.h>

RobotControllerInterface::RobotControllerInterface():
    joint_angle_threshold_(0.0)
{
    
}

RobotControllerInterface::~RobotControllerInterface ()
{
}

void RobotControllerInterface::initialize(const ros::NodeHandle &nh)
{
    ros::NodeHandle nh_ = nh;
    joint_position_command_pub_ = nh_.advertise < brics_actuator::JointPositions> ("/body_controller/position_command", 1);
    joint_velocity_command_pub_ = nh_.advertise < brics_actuator::JointVelocities> ("/body_controller/velocity_command", 1);

    joint_states_sub_ = nh_.subscribe("/joint_states", 1,
                            &RobotControllerInterface::jointStatescallback, this);

    motor_states_sub_ = nh_.subscribe("/motor_states", 1,
                            &RobotControllerInterface::motorStatescallback, this);

    torso_state_sub_ = nh_.subscribe("/zeno_imu_interface/torso_state", 1,
                            &RobotControllerInterface::torsoStatecallback, this);

    joint_angle_threshold_ = 0.04;

    torso_state_.resize(3);
    ROS_INFO("Robot controller interface is initialized");

    sleep(10);
}

void RobotControllerInterface::jointStatescallback(const sensor_msgs::JointState::ConstPtr &msg)
{
    //joint_states_map_.clear();
    for(int iter=0; iter<msg->name.size(); iter++) {
        joint_states_map_[msg->name[iter]] = msg->position[iter];
    }

    if (!on_joint_states_callback_.empty())
        on_joint_states_callback_();
}

void RobotControllerInterface::motorStatescallback(const sensor_msgs::JointState::ConstPtr &msg)
{
    for(int iter=0; iter<msg->name.size(); iter++) {
        motor_states_map_[msg->name[iter]] = msg->position[iter];
    }
}

void RobotControllerInterface::torsoStatecallback(const std_msgs::Float64MultiArray::ConstPtr &msg)
{
    if (msg->data.size() != torso_state_.size()) {
        ROS_DEBUG("Unable to store torso state.");
        ROS_DEBUG("Size of Storage buffer does not match with the torso states input");
        return;
    }

    for(int iter=0; iter<msg->data.size(); iter++) {
        torso_state_[iter] = msg->data[iter];
    }
}

void RobotControllerInterface::sendPositionCommand(const std::vector<std::string> &joint_names, 
                                            const std::vector<double> &joint_positions)
{
    brics_actuator::JointPositions positions;

    for(int iter=0; iter<joint_names.size(); iter++) {
        brics_actuator::JointValue joint_value;
        //joint_value.timeStamp
        joint_value.joint_uri = joint_names[iter];
        joint_value.unit = "rad";
        joint_value.value = joint_positions[iter];
        positions.positions.push_back(joint_value);
    }

    if(positions.positions.size() != 0)
        joint_position_command_pub_.publish(positions);
}

void RobotControllerInterface::sendVelocityCommand(const std::vector<std::string> &joint_names,
                                            const std::vector<double> &joint_velocities)
{
    brics_actuator::JointVelocities velocities;

    for(int iter=0; iter<joint_names.size(); iter++) {
        brics_actuator::JointValue joint_value;
        //joint_value.timeStamp
        joint_value.joint_uri = joint_names[iter];
        joint_value.unit = "rad-s";
        joint_value.value = joint_velocities[iter];
        velocities.velocities.push_back(joint_value);
    }
    if(velocities.velocities.size() != 0)
        joint_velocity_command_pub_.publish(velocities);
}

bool RobotControllerInterface::readJointPositions(const std::vector<std::string> &joint_names, 
                                            std::vector<double> &joint_positions)
{
    if (joint_states_map_.empty())
        return false;

    for(int iter=0; iter<joint_names.size(); iter++) {
            std::map<std::string, double>::iterator it;
            it = joint_states_map_.find(joint_names[iter]);
            if (it != joint_states_map_.end()) {
                joint_positions.push_back(it->second);
            } else {
                joint_positions.push_back(0.0);
            }
    }
    //joint_states_map_.clear();

    return true;
}

bool RobotControllerInterface::readMotorPositions(const std::vector<std::string> &joint_names, 
                                            std::vector<double> &joint_positions)
{
    if (motor_states_map_.empty())
        return false;

    for(int iter=0; iter<joint_names.size(); iter++) {
            std::map<std::string, double>::iterator it;
            it = motor_states_map_.find(joint_names[iter]);
            if (it != motor_states_map_.end()) {
                joint_positions.push_back(it->second);
            } else {
                joint_positions.push_back(0.0);
            }
    }

    return true;
}

bool RobotControllerInterface::getTorsoFeedback(std::vector<double> &torso_feedback)
{
    for(int i=0; i<torso_state_.size(); i++)
        torso_feedback.push_back(torso_state_[i]);
    return true;
}

void RobotControllerInterface::onJointStatesReceive(boost::function< void(void) > callbackFunc)
{
    on_joint_states_callback_ = boost::bind(callbackFunc);
}

bool RobotControllerInterface::isJointCommandReached(const std::vector<std::string> &joint_names, 
                                            std::vector<double> &joint_positions)
{
    if (joint_states_map_.empty())
        return false;

    for(int iter=0; iter<joint_names.size(); iter++) {
            std::map<std::string, double>::iterator it;
            it = joint_states_map_.find(joint_names[iter]);
            if (it != joint_states_map_.end()) {

                if(!checkSetPointThreshold(it->second, joint_positions[iter])) {
                    //ROS_INFO_STREAM("Joint command Threshold violated:" << joint_names[iter]);
                    //ROS_INFO_STREAM("Current : " << it->second << " Commanded : " << joint_positions[iter]);
                    //sendPositionCommand(joint_names, joint_positions);
                    return false;
                }
            }
    }
    //ROS_INFO_STREAM("Joint command reached:");
    return true;
}

bool RobotControllerInterface::checkSetPointThreshold(double current_value, double command_value)
{
    double diff  = std::fabs(std::fabs(current_value)- std::fabs(command_value));

    if(diff >= 0.0 and diff <= joint_angle_threshold_)
        return true;
    return false;
}