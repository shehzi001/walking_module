/** 
 * @file
 * @author Alexander Sherikov
 * @date 05.01.2012 17:00:43 MSK
 */

#include<oru_log.h>

test_log::test_log(const char *filename):
    is_logger_closed(false)
{
    file_op = fopen(filename, "a");
    fprintf(file_op,"hold on\n");
}

test_log::~test_log()
{
    close_log();
}


void test_log::addZMPrefPoint(const double x, const double y)
{
    ZMPref_x.push_back(x);
    ZMPref_y.push_back(y);
}
void test_log::addZMPpoint(const double x, const double y)
{
    ZMP_x.push_back(x);
    ZMP_y.push_back(y);
}
void test_log::addCoMpoint(const double x, const double y, const double z)
{
    CoM_x.push_back(x);
    CoM_y.push_back(y);
    CoM_z.push_back(z);
    //printf("addCoMpoint : %f %f;\n", x, y);
}

void test_log::addRobotCoM(const double x, const double y, int sampling_index)
{
    if (sampling_index == 0) {
        actual_CoM_x.push_back(x);
        actual_CoM_y.push_back(y);
    } else {
        expected_CoM_x.push_back(x);
        expected_CoM_y.push_back(y);
    }
    //printf("addCoMpoint : %f %f;\n", x, y);
}

void test_log::addStateError(const double x, const double y)
{
        state_error_x.push_back(x);
        state_error_y.push_back(y);
}

/*void test_log::addAngularMomentumControls(const double x, const double y)
{
        angular_momentum_controls_x.push_back(x);
        angular_momentum_controls_y.push_back(y);
}*/

void test_log::addTorsoOrientation(const double x, const double y, const double z, int source)
{
    if (source == 0) {
        torso_x.push_back(x);
        torso_y.push_back(y);
        torso_z.push_back(z);
    }else if (source == 2) {
        imu_x.push_back(x);
        imu_y.push_back(y);
        imu_z.push_back(z);
    }  /*else if (source == 1){
        torso_err_x.push_back(x);
        torso_err_y.push_back(y);
        torso_err_z.push_back(z);
    } */else {
        imu_err_x.push_back(x);
        imu_err_y.push_back(y);
        imu_err_z.push_back(z);
    }
}
void test_log::addFeetPositions (const zeno_igm &zeno)
{
    left_foot_x.push_back(zeno.left_foot_posture.data()[12]);
    left_foot_y.push_back(zeno.left_foot_posture.data()[13]);
    left_foot_z.push_back(zeno.left_foot_posture.data()[14]);
    right_foot_x.push_back(zeno.right_foot_posture.data()[12]);
    right_foot_y.push_back(zeno.right_foot_posture.data()[13]);
    right_foot_z.push_back(zeno.right_foot_posture.data()[14]);
}
void test_log::addCoMSupportChange(const double x, const double y)
{
    CoM_x_sc.push_back(x);
    CoM_y_sc.push_back(y);
}

void test_log::flushLeftFoot ()
{
    printVectors (file_op, left_foot_x, left_foot_y, left_foot_z, "LFP", "r*");
}
void test_log::flushRightFoot ()
{
    printVectors (file_op, right_foot_x, right_foot_y, right_foot_z, "RFP", "r*");
}
void test_log::flushZMP ()
{
    printVectors (file_op, ZMP_x, ZMP_y, "ZMP", "k");
}
void test_log::flushZMPref ()
{
    printVectors (file_op, ZMPref_x, ZMPref_y, "ZMPref", "ko");
}
void test_log::flushCoM ()
{
    printVectors (file_op, CoM_x, CoM_y, CoM_z,"CoM", "b");
}
void test_log::flushRobotCoM ()
{
    printVectors (file_op, actual_CoM_x, actual_CoM_y,"ActualRobotCoM", "g*");
    printVectors (file_op, expected_CoM_x, expected_CoM_y,"ExpectedCoM", "bo");
}

void test_log::flushTorsoOrientation ()
{
    printVectorsTorso (file_op, torso_x, torso_y, torso_z, "torso_orientation_kinematics", "b");
    //printVectorsTorso (file_op, torso_err_x, torso_err_y, torso_err_z, "torso_orientation_kinematics_error", "b");
    
    printVectorsTorso (file_op, imu_x, imu_y, imu_z, "torso_orientation_imu", "y");
    printVectorsTorso (file_op, imu_err_x, imu_err_y, imu_err_z, "torso_orientation_imu_error", "y");
}

void test_log::printVectorsTorso (
        FILE *file_op_, 
        std::vector<double> &data_x, 
        std::vector<double> &data_y, 
        std::vector<double> &data_z, 
        const char *name,
        const char *format)
{
    fprintf(file_op_,"%s = [\n", name);
    for (unsigned int i=0; i < data_x.size(); i++)
    {
        fprintf(file_op_, "%f %f %f;\n", data_x[i], data_y[i], data_z[i]);
    }
    fprintf(file_op_, "];\n\n"); // plot3(%s(:,1), %s(:,2), %s(:,3), '%s')\n", name, name, name, format);
}

void test_log::flushStateError()
{
    printVectors (file_op, state_error_x, state_error_y, "StateError", "b*");
}

/*void test_log::flushAngularMomentumControls()
{
    printVectors (file_op, angular_momentum_controls_x, angular_momentum_controls_y, "AngularMomentumControls", "b*");
}*/

void test_log::flushCoMSupportChange ()
{
    printVectors (file_op, CoM_x_sc, CoM_y_sc, "CoM_sc", "r*");
}

void test_log::printVectors (
        FILE *file_op_, 
        std::vector<double> &data_x, 
        std::vector<double> &data_y, 
        const char *name,
        const char *format)
{
    fprintf(file_op_,"%s = [\n", name);
    for (unsigned int i=0; i < data_x.size(); i++)
    {
        fprintf(file_op_, "%f %f;\n", data_x[i], data_y[i]);
    }
    fprintf(file_op_, "];\n\n plot(%s(:,1), %s(:,2), '%s')\n", name, name, format);
}


void test_log::printVectors (
        FILE *file_op_, 
        std::vector<double> &data_x, 
        std::vector<double> &data_y, 
        std::vector<double> &data_z, 
        const char *name,
        const char *format)
{
    fprintf(file_op_,"%s = [\n", name);
    for (unsigned int i=0; i < data_x.size(); i++)
    {
        fprintf(file_op_, "%f %f %f;\n", data_x[i], data_y[i], data_z[i]);
    }
    fprintf(file_op_, "];\n\n plot3(%s(:,1), %s(:,2), %s(:,3), '%s')\n", name, name, name, format);
}

void test_log::close_log()
{
    if(!is_logger_closed) {
        fprintf(file_op,"hold off\n");
        fclose(file_op);
        is_logger_closed = true;
    }
}