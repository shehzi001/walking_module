/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */

#include "oru_walk.h"
#include "oruw_log.h"
//#include "oruw_timer.h"

/**
 * @brief A control loop, that is executed in separate thread.
 * @attention REAL-TIME!
 */
void oru_walk::walkControl()
{
    wp.walkParametersFromROSServer(nh_); //wp.readparameters from ROS server

    initRobot();

    dcmCallbackwait();

    readSensors (zeno.state_sensor);

    WMG wmg(wp.preview_window_size,
            wp.preview_sampling_time_ms,
            wp.step_height,
            wp.bezier_weight_1,
            wp.bezier_weight_2,
            wp.bezier_inclination_1,
            wp.bezier_inclination_2);
    wmg.T_ms[0] = wp.control_sampling_time_ms;
    wmg.T_ms[1] = wp.control_sampling_time_ms;
    wmg.initFootConstraints(
         wp.distance_x1,
         wp.distance_x2,
         wp.distance_y1,
         wp.distance_y2,
         wp.support_distance_y);

    smpc::state_com CoM;

    try
    {
        if(wp.footstep_planner_type == EXTERNAL){
            initWalkPattern(wmg, step_target_array);
        } else {
            initWalkPattern(wmg);
        }

        if (wp.activate_arm_swing)
            init_arm_swing_motion(wp.preview_window_size, 1, -1);

        // error in position of the swing foot
        correctNextSupportPosition(wmg);
    }
    catch (...)
    {
        return;
    }

    if (wp.controller_type_ == WAIST_TRACKER)
        zeno.getWaistCoM (zeno.state_sensor, zeno.CoM_position);
    else if(wp.controller_type_ == COM_TRACKER)
        zeno.getCoM (zeno.state_sensor, zeno.CoM_position);

    smpc_parameters mpc(wp.preview_window_size, zeno.CoM_position[2]-wp.com_offset_);
    mpc.init_state.set(zeno.CoM_position[0], zeno.CoM_position[1]);
    CoM.set(zeno.CoM_position[0], zeno.CoM_position[1]);

    /*
     * Initiliaze reference orientation of torso from imu states and robot model
     **/
    robot_interface_->getTorsoFeedback(torso_feedback_init_);
    getWaistOrientationFromRobotModel(waist_orientation_model_init_);

    if(wp.activate_logging) {
        string temp;
        temp = wp.logger_test_case_name + std::to_string(rand());
        string logger_file_full_loc = wp.logger_file_path + temp + "_" + wp.logger_file_name;
        log.reset(new test_log(logger_file_full_loc.c_str()));
        wmg.FS2file(logger_file_full_loc, true);
        ORUW_LOG_OPEN(wp.controller_type_, wp.logger_file_path + temp);
    }
    dcm_loop_break = false;

    
    

    if (!is_simulation_running and wp.simulation) {
        initialize_simulation(wmg);
        is_simulation_running = true;
    }

    ROS_INFO("Starting Walk control cycle.");
    while(ros::ok())
    {
        jointState target_joint_state = zeno.state_model;
        jointState motor_joint_state = zeno.state_model;

        if(!dcmCallbackwait())
            break;
        readSensors (zeno.state_sensor);
        readMotorState (motor_joint_state);
        //zeno.state_sensor = zeno.state_model;

        if(wp.activate_logging) {
            ORUW_LOG_JOINTS(zeno.state_sensor, target_joint_state, motor_joint_state);
            ORUW_LOG_COM(mpc, zeno);
            ORUW_LOG_FEET(zeno);
        }

        try
        {
            if (wp.activate_state_feedback_controller) {
                feedbackError (mpc.init_state);
                //feedbackOrientationError(mpc.init_state);
            }

            if (solveMPCProblem (wmg, mpc))  // solve MPC
            {
                if (wmg.isSupportSwitchNeeded())
                {
                    correctNextSupportPosition(wmg);
                    zeno.switchSupportFoot();
                }

                int current_step_type;
                double time_left;

                wmg.getCurrentStepInfo(current_step_type, time_left);

                if((current_step_type == 3) && wp.activate_logging)
                    log->addCoMSupportChange(mpc.init_state.x(), mpc.init_state.y());

                // the old solution from is an initial guess;
                solver->get_state(CoM, 0);
                //limitFeedForwardCOM(CoM);
                if (!solveIKsendCommands (mpc, CoM, 1, wmg)) break;

                //solver->get_state(CoM, 1);
                //limitFeedForwardCOM(CoM);
                //if (!solveIKsendCommands (mpc, CoM, 2, wmg)) break;

                target_joint_state = zeno.state_model;
            }
            else
            {
                ROS_INFO("MPC problem not solved. breaking");
                break;
            }
        }
        catch (...)
        {
            ROS_INFO("walk main control loop. exception caught.");
            break;
        }
    }

    /*
    reset robot posture to home
    */
    initRobot();
    sleep(2.0);

    if(wp.activate_logging) {
        ORUW_LOG_STEPS(wmg);
        ORUW_LOG_CLOSE;

        // output
        log->flushLeftFoot ();
        log->flushRightFoot ();
        log->flushZMP ();
        log->flushZMPref ();
        log->flushCoM ();
        log->flushCoMSupportChange();
        //log->flushTorsoOrientation();
        log->flushStateError();
        //log->flushRobotCoM();
        log->close_log();
    }

    ROS_INFO("Walk cycle is finished.");
    std::cout << "Average walking control cycle time : " << (avg_control_loop_time/control_loop_counter) << std::endl;

    dcm_loop_break = true;
}

/**
 * @brief Solve the MPC problem.
 *
 * @param[in,out] wmg WMG
 * @param[in,out] mpc MPC parameters
 *
 * @return false if there is not enough steps, true otherwise.
 */
bool oru_walk::solveMPCProblem (
        WMG &wmg,
        smpc_parameters &mpc)
{
    if (wmg.formPreviewWindow(mpc) == WMG_HALT)
    {
        //ROS_DEBUG("Not enough steps to form preview window. Stopping.");
        return (false);
    }

    
    if(wp.activate_logging) {
        /*for (unsigned int j = 0; j < wp.preview_window_size; j++)
        {
            log->addZMPrefPoint(mpc.zref_x[j], mpc.zref_y[j]);
        }*/
    
        log->addZMPrefPoint(mpc.zref_x[0], mpc.zref_y[0]);
        log->addZMPrefPoint(mpc.zref_x[0], mpc.zref_y[0]);
    }

    //------------------------------------------------------
    solver->set_parameters (mpc.T, mpc.h, mpc.h[0], mpc.angle, mpc.zref_x, mpc.zref_y, mpc.lb, mpc.ub);
    solver->form_init_fp (mpc.fp_x, mpc.fp_y, mpc.init_state, mpc.X);
    solver->solve();
    solver->get_next_state(mpc.init_state);

    //------------------------------------------------------
    if(wp.activate_logging)
        ORUW_LOG_SOLVER_INFO;

    return (true);
}

/**
 * @brief Solve inverse kinematics and send commands to the controllers.
 *
 * @param[in] mpc MPC parameters
 * @param[in] CoM  CoM position
 * @param[in] control_loop_num number of control loops in future (>= 1).
 * @param[in,out] wmg WMG
 */
bool oru_walk::solveIKsendCommands (
        const smpc_parameters &mpc,
        const smpc::state_com &CoM,
        const int control_loop_num,
        WMG &wmg)
{
    smpc::state_zmp next_ZMP;
    solver->get_state (next_ZMP, control_loop_num-1);

    // support foot and swing foot position/orientation
    wmg.getFeetPositions (
            control_loop_num * wp.control_sampling_time_ms, 
            zeno.left_foot_posture.data(), 
            zeno.right_foot_posture.data());

    double height = 0.0;
    if (wp.enable_variable_com) {
        Transform<double,3> foot_posture;
        if(zeno.support_foot == IGM_SUPPORT_RIGHT) {
            Matrix4d posture = Matrix4d::Map(zeno.left_foot_posture.data());
            height = posture(2,3)*wp.com_max_height;
        } else {
            Matrix4d posture = Matrix4d::Map(zeno.right_foot_posture.data());
            height = posture(2,3)*wp.com_max_height;
        }
    }

    // hCoM is constant!
    zeno.setCoM(CoM.x(), CoM.y(), mpc.hCoM+height);

    int iter_num = -1;
    // inverse kinematics
    if (wp.controller_type_ == WAIST_TRACKER)
        iter_num = zeno.igmWaist (
                ref_joint_angles, 
                wp.igm_mu, 
                wp.igm_tol, 
                wp.igm_max_iter);
    else if(wp.controller_type_ == COM_TRACKER) {
        if (wp.igm_type == AUTO) {
            iter_num = zeno.igm (
                    ref_joint_angles, 
                    wp.igm_mu, 
                    wp.igm_tol, 
                    wp.igm_max_iter);
        } else if (wp.igm_type == CONSTRAIN_WAIST_PITCH) {
            iter_num = zeno.igmWithWaistPitchConstraint (
                    ref_joint_angles, 
                    wp.igm_mu, 
                    wp.igm_tol, 
                    wp.igm_max_iter,
                    wp.igm_waist_pitch_constraint_angle,
                    wp.igm_waist_pitch_samples,
                    wp.igm_waist_pitch_sampling_step);
        } else if (wp.igm_type == CONSTRAIN_WAIST_ROLL_PITCH) {
            iter_num = zeno.igmWithWaistRollPitchConstraint (
                    ref_joint_angles, 
                    wp.igm_mu, 
                    wp.igm_tol, 
                    wp.igm_max_iter);
        }
    }

    if (iter_num < 0)
    {
        ROS_INFO( "IK failed");
        return false;
    }
    
    int failed_joint = zeno.state_model.checkJointBounds();
    if (failed_joint >= 0)
    {
        return false;
    }
    else {
        if(wp.activate_logging) {
            log->addCoMpoint (CoM.x(), CoM.y(), mpc.hCoM+height);
            log->addFeetPositions (zeno);
            log->addZMPpoint (next_ZMP.x(), next_ZMP.y());
        }
        try
        {  
            if (wp.activate_arm_swing && control_loop_num == 2) {
                    int current_step_type;
                    double time_left;

                    wmg.getCurrentStepInfo(current_step_type, time_left);

                    double l_shoulder_pitch;
                    double r_shoulder_pitch;
                    get_arm_swing_motion(current_step_type, l_shoulder_pitch,
                                                            r_shoulder_pitch);

                    zeno.state_model.q[L_SHOULDER_PITCH] = l_shoulder_pitch;
                    zeno.state_model.q[R_SHOULDER_PITCH] = r_shoulder_pitch;    
            }

            for (int i = 0; i < LOWER_JOINTS_NUM; i++)
            {
                joint_commands[i] = zeno.state_model.q[i];
                ref_joint_angles[i] = joint_commands[i];
            }
            int step_type;
            double time_left;
            wmg.getCurrentStepInfo(step_type, time_left);



            if(step_type == FS_TYPE_SS_L) {
                /*joint_commands[L_HIP_ROLL] -= 0.2;
                joint_commands[L_ANKLE_ROLL] += 0.02;*/
                //joint_commands[R_ANKLE_PITCH] -= 0.02;
                /*if (time_left > 160.0 && time_left < 300)
                    joint_commands[R_HIP_ROLL] -= 0.2;
                    setJointStiffness(true);
                /*else
                    setJointStiffness(false);*/
            }
            else if(step_type == FS_TYPE_SS_R) {
                /*joint_commands[L_HIP_ROLL] -= 0.2;
                joint_commands[L_ANKLE_ROLL] += 0.2;*/
                //joint_commands[L_ANKLE_PITCH] -= 0.02;
                /*if (time_left > 160.0 && time_left < 300)
                    joint_commands[L_HIP_ROLL] += 0.2;
                    setJointStiffness(true);
                else
                    setJointStiffness(false);*/
            } else if(step_type == FS_TYPE_DS)  {
                //setJointStiffness(true);
            }

            joint_commands[L_SHOULDER_PITCH] = zeno.state_model.q[L_SHOULDER_PITCH];
            joint_commands[R_SHOULDER_PITCH] = zeno.state_model.q[R_SHOULDER_PITCH];

            if (is_simulation_running and wp.simulation) {
                simulate();
            }

            robot_interface_->sendPositionCommand(joint_names, joint_commands);
        }
        catch (...)
        {
            ROS_INFO("Cannot set joint angles!");
        }
    }

    return true;
}

