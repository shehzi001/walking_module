/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */


#include "oru_walk.h"


/**
 * @brief Wake up walk control thread periodically.
 */

bool oru_walk::dcmCallbackwait()
{
    ros::Rate loop_rate(100);

    double diff = 0.0;
    bool return_msg = false;

    double start = ros::Time::now().toSec();
    dcm_loop_counter = 0; 
    while(ros::ok()) {
        ros::spinOnce();
        diff = (ros::Time::now().toSec() - start)*1000;
        /*if(diff > 250) {
            control_loop_counter++;
            break;
        }*/

        if(diff >= wp.control_loop_time_ms)  {
            dcm_loop_counter = 0;
            control_loop_counter++;
            return_msg = true;
            break;
        }
        
        loop_rate.sleep();
    }

    avg_control_loop_time +=  diff;
    return return_msg;
}



/**
 * @brief Initialize solver
 */
void oru_walk::initSolver()
{
    if (solver != NULL)
    {
        delete solver;
        solver = NULL;
    }
    if (wp.mpc_solver_type == SOLVER_TYPE_AS)
    {
        solver = new smpc::solver_as (
                wp.preview_window_size,
                wp.mpc_gain_position,
                wp.mpc_gain_velocity,
                wp.mpc_gain_acceleration,
                wp.mpc_gain_jerk,
                wp.mpc_as_tolerance,
                wp.mpc_as_max_activate,
                wp.mpc_as_use_downdate,
                false); // objective
    }
    else if (wp.mpc_solver_type == SOLVER_TYPE_IP)
    {
        solver = new smpc::solver_ip (
                wp.preview_window_size,
                wp.mpc_gain_position,
                wp.mpc_gain_velocity,
                wp.mpc_gain_acceleration,
                wp.mpc_gain_jerk,
                wp.mpc_ip_tolerance_int,
                wp.mpc_ip_tolerance_ext,
                wp.mpc_ip_t,
                wp.mpc_ip_mu,
                wp.mpc_ip_bs_alpha,
                wp.mpc_ip_bs_beta,
                wp.mpc_ip_max_iter,
                (smpc::backtrackingSearchType) wp.mpc_ip_bs_type,
                false); // objective
    }
//    smpc::enable_fexceptions();
}



/**
 * @brief The position of the next support foot may change from the targeted, 
 * this function moves it to the right place.
 */
void oru_walk::correctNextSupportPosition(WMG &wmg)
{
    Transform<double,3> swing_foot_posture;
    zeno.getSwingFootPosture (zeno.state_sensor, swing_foot_posture.data());
    wmg.changeNextSSPosition (swing_foot_posture.data(), wp.set_support_z_to_zero);
}

/**
 * Get waist orientation from robot model
 */
void oru_walk::getWaistOrientationFromRobotModel(std::vector<double> &waist_orientation)
{
    Transform<double,3> foot_posture;
    zeno.getWaistPosture(zeno.state_sensor, foot_posture.data());

    Matrix4d posture = Matrix4d::Map(foot_posture.data());
    Matrix3d rotation = posture.corner(TopLeft,3,3);

    Eigen::Matrix<double,3,1> euler = rotation.eulerAngles(0,1,2);
    if (euler[0] > 1.0)
            euler[0] -= PI;

    if (euler[1] > 2.0)
        euler[1] -= PI;
    else if (euler[1] < -2.0)
        euler[1] += PI;

    if (euler[2] > 2.0)
        euler[2] -= PI;
    else if (euler[2] < -2.0)
        euler[2] += PI;

    waist_orientation.push_back(euler[0]);
    waist_orientation.push_back(euler[1]);
    waist_orientation.push_back(euler[2]);
}

