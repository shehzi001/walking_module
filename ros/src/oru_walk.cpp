/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 */

#include "oru_walk.h"

/**
 * Constructor for oru_walk object
 * @param broker The parent broker
 * @param name The name of the module
 */

oru_walk::oru_walk(const string &name, const ros::NodeHandle &nh):
    dcm_loop_break(false),nh_(nh), default_joint_velocities_(0.523),
    is_step_command_recieved_(false), is_simulation_running(false) //,log("zeno_test_run.m")
{
    solver = NULL;
    robot_interface_.reset(new RobotControllerInterface());
    robot_interface_->initialize(nh_);
    step_command_sub_ = nh_.subscribe("/footstep_planner_interface/footsteps", 100,
                                        &oru_walk::stepsCallBack, this);

    event_in_sub_ = nh_.subscribe("/walk_module/event_in", 1, &oru_walk::eventInCallBack, this);
}



/**
 * Destructor for oru_walk object
 */
oru_walk::~oru_walk()
{
    if (solver != NULL)
    {
        delete solver;
        solver = NULL;
    }
}

/**
 * Runs a cycle and waits for the input to start.
 */
void oru_walk::executeCycle()
{
    ROS_INFO("Waiting for the footstep command..");

    // initialize walk control parameters and solvers
    if(!initwalkControl()) return;

    ros::Rate loop_rate(10);
    while(ros::ok()) {
        ros::spinOnce();
        if(is_step_command_recieved_) {
            is_step_command_recieved_ = false;
            walkControl();
            ROS_INFO("Waiting for the footstep command..");
        }
        
        loop_rate.sleep();
    }

    ROS_INFO("Stopped on user's request.");
}
