/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */


#include "oru_walk.h"

void oru_walk::init_arm_swing_motion(int N, int left_arm_direction, int right_arm_direction)
{
    //int N = 2*N_in;
    arm_swing_motion_buffer.clear();
    arm_swing_motion_buffer.resize(N);
    double arm_swing_min = wp.arm_swing_min;
    double arm_swing_max = wp.arm_swing_max;
    double step_size = (arm_swing_max-arm_swing_min)/N;
    double step_increment = 0.0;

    for (int i = 0; i < N; i++)
    {
        arm_swing_motion_buffer[i] = arm_swing_min+step_increment;
        step_increment += step_size;
    }
    left_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
    right_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
    left_arm_swing_direction_ = left_arm_direction;
    right_arm_swing_direction_ = right_arm_direction;
}

void oru_walk::get_arm_swing_motion(int support_foot, double &left_arm, double &right_arm)
{
    if(support_foot == 1 or support_foot == 2) {

        if (left_arm_swing_index_ < 0) {
            left_arm_swing_direction_ = 1;
            left_arm_swing_index_ = 0;
        } else if (left_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
            left_arm_swing_direction_ = -1;
            left_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
        }

        if (right_arm_swing_index_ < 0) {
            right_arm_swing_direction_ = 1;
            right_arm_swing_index_ = 0;
        } else if (right_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
            right_arm_swing_direction_ = -1;
            right_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
        }
    }

    left_arm = arm_swing_motion_buffer[left_arm_swing_index_];
    right_arm = arm_swing_motion_buffer[right_arm_swing_index_];

    if (support_foot == 1 or support_foot == 2) {
        left_arm_swing_index_ += left_arm_swing_direction_;
        right_arm_swing_index_ += right_arm_swing_direction_;
    }
}