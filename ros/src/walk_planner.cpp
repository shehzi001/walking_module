/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */

#include "oru_walk.h"

/**
 * @brief Initialize selected walk pattern
 */
void oru_walk::initWalkPattern(WMG &wmg, humanoid_nav_msgs::StepTargetArray step_array)
{
    // each step is defined relatively to the previous step
    const double step_x = wp.step_length;                        // relative X position
    const double step_y = wmg.def_constraints.support_distance_y;// relative Y position

    int first_planned_swing_foot_ = step_array.footsteps[0].leg;

    if (first_planned_swing_foot_ == 1) {
        std::cout << "selected support foot is right" << std::endl;
        double right_foot_init_pose_x =  step_array.footsteps[0].pose.x;
        double right_foot_init_pose_y =   step_array.footsteps[0].pose.y-step_y;
        std::cout << "init pose" << right_foot_init_pose_x << ", " << right_foot_init_pose_y << ", " << step_array.footsteps[0].pose.theta << std::endl;

        // support foot position and orientation
        /*zeno.init (
                IGM_SUPPORT_RIGHT,
                //0.0, 0.04212, 0.0, // position
                right_foot_init_pose_x , right_foot_init_pose_y, 0.0, // position 
                0.0, 0.0, step_array.footsteps[0].pose.theta);  // orientation*/

        zeno.init (
            IGM_SUPPORT_RIGHT,
            //0.0, 0.04212, 0.0, // position
            0.0, -0.044810, 0.0, // position  
            0.0, 0.0, 0.0);  // orientation
        // swing foot position
        zeno.getSwingFootPosture (zeno.state_sensor, zeno.left_foot_posture.data());
    } else {
        std::cout << "selected support foot is left" << std::endl;
        double left_foot_init_pose_x =  step_array.footsteps[0].pose.x;
        double left_foot_init_pose_y =   step_y + step_array.footsteps[0].pose.y;

        // support foot position and orientation
        /*zeno.init (
                IGM_SUPPORT_LEFT,
                //0.0, 0.04212, 0.0, // position
                left_foot_init_pose_x , left_foot_init_pose_y, 0.0, // position 
                0.0, 0.0, step_array.footsteps[0].pose.theta);  // orientation*/
        zeno.init (
                IGM_SUPPORT_LEFT,
                //0.0, 0.04212, 0.0, // position
                0.0, 0.044810, 0.0, // position  
                 0.0, 0.0, 0.0);  // orientation
        // swing foot position
        zeno.getSwingFootPosture (zeno.state_sensor, zeno.right_foot_posture.data());
    }


    switch (wp.walk_pattern)
    {
        case WALK_PATTERN_STRAIGHT:
            initWalkPattern_Straight_Abs(wmg, step_array);
            break;
        case WALK_PATTERN_DIAGONAL:
            initWalkPattern_Diagonal(wmg);
            break;
        case WALK_PATTERN_CIRCULAR:
            initWalkPattern_Circular(wmg);
            break;
        case WALK_PATTERN_BACK:
            initWalkPattern_Back(wmg);
            break;
        default:
            ROS_ERROR("Unknown walk pattern.");
            break;
    }
}

/**
 * @brief Initializes walk pattern
 */
void oru_walk::initWalkPattern_Straight_Abs(WMG &wmg, humanoid_nav_msgs::StepTargetArray step_array)
{
    // each step is defined relatively to the previous step
    const double step_x = wp.step_length;                        // relative X position
    const double step_y = wmg.def_constraints.support_distance_y-0.005;// relative Y position

    int first_planned_swing_foot_ = step_array.footsteps[0].leg;
    if (first_planned_swing_foot_ == 0) {
        double left_foot_init_pose_x =  step_array.footsteps[0].pose.x;
        double left_foot_init_pose_y =  step_y + step_array.footsteps[0].pose.y;

        double ds_init_pose_x =  step_array.footsteps[0].pose.x;
        double ds_init_pose_y =  (step_array.footsteps[0].pose.y + left_foot_init_pose_y)/2;


        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstepAbs(left_foot_init_pose_x , left_foot_init_pose_y, step_array.footsteps[0].pose.theta, FS_TYPE_SS_L);

        // Initial double support
        wmg.setFootstepParametersMS (6*400, 0, 0);
        wmg.addFootstepAbs(ds_init_pose_x, ds_init_pose_y, step_array.footsteps[0].pose.theta, FS_TYPE_DS);

        // all subsequent steps have normal feet size
        wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
        wmg.addFootstepAbs(step_array.footsteps[0].pose.x   , step_array.footsteps[0].pose.y, step_array.footsteps[0].pose.theta,FS_TYPE_SS_R);

        wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);

        int size_steps = step_array.footsteps.size();

        for(int idx = 1; idx < size_steps; idx++)
            wmg.addFootstepAbs(step_array.footsteps[idx].pose.x   , step_array.footsteps[idx].pose.y, 
                step_array.footsteps[idx].pose.theta);
    } else {

        double right_foot_init_pose_x =  step_array.footsteps[0].pose.x;
        double right_foot_init_pose_y =  step_array.footsteps[0].pose.y - step_y;

        double ds_init_pose_x =  step_array.footsteps[0].pose.x;
        double ds_init_pose_y =  (step_array.footsteps[0].pose.y + right_foot_init_pose_y)/2;


        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstepAbs(right_foot_init_pose_x , right_foot_init_pose_y, step_array.footsteps[0].pose.theta, FS_TYPE_SS_R);

        // Initial double support
        wmg.setFootstepParametersMS (6*400, 0, 0);
        wmg.addFootstepAbs(ds_init_pose_x, ds_init_pose_y, step_array.footsteps[0].pose.theta, FS_TYPE_DS);

        // all subsequent steps have normal feet size
        wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
        wmg.addFootstepAbs(step_array.footsteps[0].pose.x   , step_array.footsteps[0].pose.y, step_array.footsteps[0].pose.theta,FS_TYPE_SS_L);

        wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);

        int size_steps = step_array.footsteps.size();

        for(int idx = 1; idx < size_steps; idx++)
            wmg.addFootstepAbs(step_array.footsteps[idx].pose.x   , step_array.footsteps[idx].pose.y, 
                step_array.footsteps[idx].pose.theta);


        double ds_last_pose_x =  step_array.footsteps[size_steps-1].pose.x;
        double ds_last_pose_y =  (step_array.footsteps[size_steps-1].pose.y + step_array.footsteps[size_steps-2].pose.y)/4;
        double ss_last_pose_y =  (step_array.footsteps[size_steps-1].pose.y + step_array.footsteps[size_steps-2].pose.y)/2;

        wmg.setFootstepParametersMS (600, 0, 0);
        wmg.addFootstepAbs(step_array.footsteps[size_steps-1].pose.x   , ds_last_pose_y, 
                            step_array.footsteps[size_steps-1].pose.theta, FS_TYPE_DS);

        wmg.setFootstepParametersMS (0, 0, 0);
        if(step_array.footsteps[size_steps-1].leg == 0) {
            wmg.addFootstepAbs(step_array.footsteps[size_steps-1].pose.x   , step_array.footsteps[size_steps-1].pose.y, 
                        step_array.footsteps[size_steps-1].pose.theta, FS_TYPE_SS_L);
        } else {
            wmg.addFootstepAbs(step_array.footsteps[size_steps-1].pose.x   , step_array.footsteps[size_steps-1].pose.y, 
                        step_array.footsteps[size_steps-1].pose.theta, FS_TYPE_SS_R);
        }

    }

    /*wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

    // Initial double support
    wmg.setFootstepParametersMS (6*400, 0, 0);
    wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


    // all subsequent steps have normal feet size
    wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0);

    wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
    wmg.addFootstep(step_x,  step_y, 0.0);

    for (int i = 0; i < wp.step_pairs_number; i++)
    {
        wmg.addFootstep(step_x, -step_y, 0.0);
        wmg.addFootstep(step_x,  step_y, 0.0);
    }

    wmg.setFootstepParametersMS (5.5*400, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
    wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);*/
}