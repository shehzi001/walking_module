/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 */

#include "oru_walk.h"


// Initialisation of joint commands, joint names , velocities, ...
/**
 * @brief 
 */
void oru_walk::init()
{
    joint_names.clear();
    joint_names.resize(JOINTS_NUM);
    joint_names[HEAD_PITCH]       = "neck_pitch";
    joint_names[HEAD_YAW]         = "neck_yaw";
    joint_names[HEAD_ROLL]        = "neck_roll";

    joint_names[L_ANKLE_PITCH]    = "left_ankle_pitch";
    joint_names[L_ANKLE_ROLL]     = "left_ankle_roll";
    joint_names[L_ELBOW_ROLL]     = "left_elbow_roll";
    joint_names[L_ELBOW_YAW]      = "left_elbow_yaw";
    joint_names[L_HIP_PITCH]      = "left_hip_pitch";
    joint_names[L_HIP_ROLL]       = "left_hip_roll";
    joint_names[L_HIP_YAW]        = "left_hip_yaw";
    joint_names[L_KNEE_PITCH]     = "left_knee_pitch";
    joint_names[L_SHOULDER_PITCH] = "left_shoulder_pitch";
    joint_names[L_SHOULDER_ROLL]  = "left_shoulder_roll";
    joint_names[L_WRIST_YAW]      = "left_wrist_yaw";

    joint_names[R_ANKLE_PITCH]    = "right_ankle_pitch";
    joint_names[R_ANKLE_ROLL]     = "right_ankle_roll";
    joint_names[R_ELBOW_ROLL]     = "right_elbow_roll";
    joint_names[R_ELBOW_YAW]      = "right_elbow_yaw";
    joint_names[R_HIP_PITCH]      = "right_hip_pitch";
    joint_names[R_HIP_ROLL]       = "right_hip_roll";
    joint_names[R_HIP_YAW]        = "right_hip_yaw";
    joint_names[R_KNEE_PITCH]     = "right_knee_pitch";
    joint_names[R_SHOULDER_PITCH] = "right_shoulder_pitch";
    joint_names[R_SHOULDER_ROLL]  = "right_shoulder_roll";
    joint_names[R_WRIST_YAW]      = "right_wrist_yaw";

    joint_names[TORSO_YAW]         = "waist";

    for (int i=0; i< JOINTS_NUM; i++) {
        joint_indices_[i] = i;
    }

    walking_control_joint_names.clear(); 
    walking_control_joint_names.resize(LOWER_JOINTS_NUM); 
    for(int i=0; i < LOWER_JOINTS_NUM; i++) {
        walking_control_joint_names[i] = joint_names[i];
    }

    initJointAngles();
}


void oru_walk::initJointAngles()
{
    init_joint_angles.clear();
    init_joint_angles.resize(JOINTS_NUM);

    double default_joint_velocities = default_joint_velocities_;
        
    init_joint_angles[L_HIP_ROLL]       =       0.0;
    init_joint_angles[L_HIP_YAW]        =       0.0;
    init_joint_angles[L_HIP_PITCH]      =       -0.35;//-0.436332;
    init_joint_angles[L_KNEE_PITCH]     =       0.75;//0.436332;
    init_joint_angles[L_ANKLE_PITCH]    =       -0.416; //-0.436332/4;
    init_joint_angles[L_ANKLE_ROLL]     =       0.0;

    init_joint_angles[R_HIP_ROLL]       =       0.0;
    init_joint_angles[R_HIP_YAW]        =       0.0;
    init_joint_angles[R_HIP_PITCH]      =       -0.35; //-0.436332;
    init_joint_angles[R_KNEE_PITCH]     =       0.75; //0.436332;
    init_joint_angles[R_ANKLE_PITCH]    =       -0.416; //-0.436332/4;
    init_joint_angles[R_ANKLE_ROLL]     =       0.0;

    init_joint_angles[L_SHOULDER_PITCH] =       1.0;//0.78539;
    init_joint_angles[L_SHOULDER_ROLL]  =       0.25;//0.78539;
    init_joint_angles[L_ELBOW_ROLL]     =       -0.1;
    init_joint_angles[L_ELBOW_YAW]      =       -0.1;//-0.78539;
    init_joint_angles[L_WRIST_YAW]      =       0.0;

    init_joint_angles[R_SHOULDER_PITCH] =       1.0;//0.78539;
    init_joint_angles[R_SHOULDER_ROLL]  =       -0.25;//-0.78539;
    init_joint_angles[R_ELBOW_ROLL]     =       0.1;
    init_joint_angles[R_ELBOW_YAW]      =       0.1;//0.78539;
    init_joint_angles[R_WRIST_YAW]      =       0.0;

    init_joint_angles[HEAD_PITCH]       =       0.0;
    init_joint_angles[HEAD_YAW]         =       0.0;
    init_joint_angles[HEAD_ROLL]        =       0.0;

    init_joint_angles[TORSO_YAW]        =       0.0;


    // Initialize joint velocities
    init_joint_velocities.clear();
    init_joint_velocities.resize(JOINTS_NUM);
    init_joint_velocities[L_HIP_ROLL]       =       0.0;
    init_joint_velocities[L_HIP_YAW]        =       0.0;
    init_joint_velocities[L_HIP_PITCH]      =       0.0;
    init_joint_velocities[L_KNEE_PITCH]     =       0.0;
    init_joint_velocities[L_ANKLE_PITCH]    =       0.0;
    init_joint_velocities[L_ANKLE_ROLL]     =       0.0;

    init_joint_velocities[R_HIP_ROLL]       =       0.0;
    init_joint_velocities[R_HIP_YAW]        =       0.0;
    init_joint_velocities[R_HIP_PITCH]      =       0.0;
    init_joint_velocities[R_KNEE_PITCH]     =       0.0;
    init_joint_velocities[R_ANKLE_PITCH]    =       0.0;
    init_joint_velocities[R_ANKLE_ROLL]     =       0.0;

    init_joint_velocities[L_SHOULDER_PITCH] =       default_joint_velocities_;
    init_joint_velocities[L_SHOULDER_ROLL]  =       default_joint_velocities_;
    init_joint_velocities[L_ELBOW_ROLL]     =       default_joint_velocities_;
    init_joint_velocities[L_ELBOW_YAW]      =       default_joint_velocities_;
    init_joint_velocities[L_WRIST_YAW]      =       default_joint_velocities_;

    init_joint_velocities[R_SHOULDER_PITCH] =       default_joint_velocities_;
    init_joint_velocities[R_SHOULDER_ROLL]  =       default_joint_velocities_;
    init_joint_velocities[R_ELBOW_ROLL]     =       default_joint_velocities_;
    init_joint_velocities[R_ELBOW_YAW]      =       default_joint_velocities_;
    init_joint_velocities[R_WRIST_YAW]      =       default_joint_velocities_;

    init_joint_velocities[HEAD_PITCH]       =       0.0;
    init_joint_velocities[HEAD_YAW]         =       0.0;
    init_joint_velocities[HEAD_ROLL]        =       0.0;

    init_joint_velocities[TORSO_YAW]        =       default_joint_velocities_;
}

/**
 * @brief Initialize commands, that will be sent to DCM.
 */

void oru_walk::initWalkCommands()
{
    joint_commands.clear();
    joint_commands.resize(JOINTS_NUM);
    joint_commands = init_joint_angles;

    for (int i = 0; i < LOWER_JOINTS_NUM; i++)
    {
        ref_joint_angles[i] = joint_commands[i];
    }
}


/**
 * @brief  Set desired Joint command and joint velocities.
 */
void oru_walk::initRobot()
{
    initWalkCommands();

    setJointStiffness(true);
    sleep(1.0);
    robot_interface_->sendPositionCommand(joint_names, joint_commands);
    sleep(2.0);
    setJointStiffness(false);
    sleep(1.0);
    ROS_INFO("Robot is initilized in walk configuration");
}


/**
 *  Set stiffnes of the joint
 */ 
void oru_walk::setJointStiffness(bool stiffness)
{
    for(int i=0; i< LOWER_JOINTS_NUM; i++) {
        if (stiffness)
            init_joint_velocities[i] = default_joint_velocities_;
        else {
            init_joint_velocities[i] = 0.0;
        }
    }

    robot_interface_->sendVelocityCommand(joint_names, init_joint_velocities);
}


void oru_walk::dcmCallback()
{
    // read from sensor and update joint values
    if(dcm_loop_counter < 3)
        dcm_loop_counter++;

    /*if (dcm_loop_counter % (wp.control_sampling_time_ms / wp.dcm_sampling_time_ms) == 0)
    {
        ROS_INFO ("dcm callback satisfied.");
        //last_dcm_time_ms = *last_dcm_time_ms_ptr + wp.dcm_time_shift_ms;
    }*/
}

bool oru_walk::initwalkControl()
{
    sleep(1.0);

    avg_control_loop_time = 0.0;
    control_loop_counter = 0;

    init();

    initRobot();

    // register callback
    dcm_loop_counter = 0;
    try
    {
       robot_interface_->onJointStatesReceive(boost::bind(&oru_walk::dcmCallback, this));
    }
    catch (...)
    {
        ROS_ERROR("Callback registration failed!");
        return false;
    }

    sleep(3.0);

    //wp.walkParametersInitialize(); //wp.readparameters
    wp.walkParametersFromROSServer(nh_); //wp.readparameters from ROS server

    initSolver();

    return true;
}