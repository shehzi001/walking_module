/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface_test.cpp
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */

#include <ros/ros.h>
#include "oru_walk.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "walking_controller_node");

    ros::NodeHandle nh_("~");
    sleep(1.0);

    oru_walk zeno_walk("walking_module", nh_);
    ROS_INFO("Walking module is initilized");
    zeno_walk.executeCycle();

    sleep(1.0);

    return 0;
}
