/**
 * @file
 * @author Alexander Sherikov
 * @date 03.01.2012 19:17:44 MSK
 */


#include "oruw_log.h"


#ifdef ORUW_LOG_ENABLE
oruw_log *oruw_log_instance = NULL;


oruw_log::oruw_log (int controller_type, string file_loc):
    controller_type_(controller_type)
{
    //string rand_str = std::to_string(rand())+"_";
    string oru_joint_log = file_loc + "_joints.m";
    string oru_com_log = file_loc  + "_com.m";
    string oru_feet_log = file_loc  +  "_feet.m";
    string oru_messages_log = file_loc  +  "_messages.log";
    FJointsLog = fopen (oru_joint_log.c_str(), "w");
    FCoMLog = fopen (oru_com_log.c_str(), "w");
    FFeetLog = fopen (oru_feet_log.c_str(), "w");
    FMessages = fopen (oru_messages_log.c_str(), "w");

    fprintf (FJointsLog, "%s ", "FJointsLog = [\n");
    fprintf (FCoMLog, "%s ", "FCoMLog = [\n");
    fprintf (FFeetLog, "%s ", "FFeetLog = [\n");
}


oruw_log::~oruw_log ()
{
    fprintf (FJointsLog, "%s ", "\n];\n");
    fprintf (FJointsLog, "%s ", "total_joints = 12;\n");
    fprintf (FJointsLog, "%s ", "joint_name = 'hip_roll';\n");
    fprintf (FJointsLog, "%s ", "total_leg_joints = 6;\n");
    fprintf (FJointsLog, "%s ", "joint_id = 1; %left hip pich \n");
    fprintf (FJointsLog, "%s ", "left_leg_joint = FJointsLog(:, joint_id); \n");
    fprintf (FJointsLog, "%s ", "left_leg_motor = FJointsLog(:, joint_id+total_joints);\n\n");

    fprintf (FJointsLog, "%s ", "joint_id = joint_id + total_leg_joints; %right leg\n");
    fprintf (FJointsLog, "%s ", "right_leg_joint = FJointsLog(:, joint_id);\n");
    fprintf (FJointsLog, "%s ", "right_leg_motor = FJointsLog(:, joint_id+total_joints);\n\n");

    fprintf (FJointsLog, "%s ", "figure_number = 2;\n");
    fprintf (FJointsLog, "%s ", "figure (figure_number)\n");
    fprintf (FJointsLog, "%s ", "plot(left_leg_joint, 'b')\n");
    fprintf (FJointsLog, "%s ", "grid on; %axis equal\n");
    fprintf (FJointsLog, "%s ", "hold on;\n");
    fprintf (FJointsLog, "%s ", "plot(left_leg_motor, 'bo')\n");
    fprintf (FJointsLog, "%s ", "legend('Joint','Motor')\n");
    fprintf (FJointsLog, "%s ", "title('Left leg angle variation')\n");
    fprintf (FJointsLog, "%s ", "% hold off;\n\n");

    fprintf (FJointsLog, "%s ", "% figure (figure_number+1)\n");
    fprintf (FJointsLog, "%s ", "plot(right_leg_joint,'r')\n");
    fprintf (FJointsLog, "%s ", "grid on; %axis equal\n");
    fprintf (FJointsLog, "%s ", "hold on;\n");
    fprintf (FJointsLog, "%s ", "plot(right_leg_motor, 'ro')\n");
    fprintf (FJointsLog, "%s ", "legend('Joint','Motor')\n");
    fprintf (FJointsLog, "%s ", "title('Right leg angle variation')\n");
    fprintf (FJointsLog, "%s ", "hold off;\n");

    fprintf (FCoMLog, "%s ", "\n];\n");
    fprintf (FCoMLog, "%s ", "oru_com_x = FCoMLog(:, 1);\n");
    fprintf (FCoMLog, "%s ", "oru_com_x_model = FCoMLog(:, 4);\n");
    fprintf (FCoMLog, "%s ", "oru_com_y = FCoMLog(:, 2);\n");
    fprintf (FCoMLog, "%s ", "oru_com_y_model = FCoMLog(:, 5);\n\n");

    fprintf (FCoMLog, "%s ", "figure_number = 4;\n");
    fprintf (FCoMLog, "%s ", "figure (figure_number)\n");
    fprintf (FCoMLog, "%s ", "plot(oru_com_x, 'r*')\n");
    fprintf (FCoMLog, "%s ", "grid on; %axis equal\n");
    fprintf (FCoMLog, "%s ", "hold on;\n");
    fprintf (FCoMLog, "%s ", "plot(oru_com_x_model, 'bo')\n");
    fprintf (FCoMLog, "%s ", "legend('smpc state','model state')\n");
    fprintf (FCoMLog, "%s ", "title('Graph of X-Component of CoM SMPC state vs CoM Model state')\n");
    fprintf (FCoMLog, "%s ", "hold off;\n\n");

    fprintf (FCoMLog, "%s ", "figure (figure_number+1)\n");
    fprintf (FCoMLog, "%s ", "plot(oru_com_y,'r*')\n");
    fprintf (FCoMLog, "%s ", "grid on; %axis equal\n");
    fprintf (FCoMLog, "%s ", "hold on;\n");
    fprintf (FCoMLog, "%s ", "plot(oru_com_y_model, 'bo')\n");
    fprintf (FCoMLog, "%s ", "legend('smpc state','model state')\n");
    fprintf (FCoMLog, "%s ", "title('Graph of Y-Component of CoM SMPC state vs CoM Model state')\n");
    fprintf (FCoMLog, "%s ", "hold off;\n");

    fprintf (FFeetLog, "%s ", "\n];");

    //close files
    fclose (FJointsLog);
    fclose (FCoMLog);
    fclose (FFeetLog);
    fclose (FMessages);
}


void oruw_log::logJointValues(
        const jointState& state_sensor,
        const jointState& state_expected,
        const jointState& motor_sensor)
{
    for (int i = 0; i < LOWER_JOINTS_NUM; i++)
    {
        fprintf (FJointsLog, "%f ", state_sensor.q[i]);
    }
    fprintf (FJointsLog, "    ");

    for (int i = 0; i < LOWER_JOINTS_NUM; i++)
    {
        fprintf (FJointsLog, "%f ", motor_sensor.q[i]);
    }
    fprintf (FJointsLog, "    ");

    for (int i = 0; i < LOWER_JOINTS_NUM; i++)
    {
        fprintf (FJointsLog, "%f ", state_expected.q[i]);
    }
    fprintf (FJointsLog, ";\n");
}


void oruw_log::logCoM(
        smpc_parameters &mpc,
        zeno_igm& zeno)
{
    fprintf (FCoMLog, "%f %f %f    ", mpc.init_state.x(), mpc.init_state.y(), mpc.hCoM);


    double CoM[POSITION_VECTOR_SIZE];

    if (controller_type_ == 0)
        zeno.getWaistCoM(zeno.state_sensor, CoM);
    else
        zeno.getCoM(zeno.state_sensor, CoM);

    fprintf (FCoMLog, "%f %f %f ;\n", CoM[0], CoM[1], CoM[2]);
}


void oruw_log::logFeet(zeno_igm& zeno)
{
    double l_expected[POSITION_VECTOR_SIZE];
    double l_real[POSITION_VECTOR_SIZE];
    double r_expected[POSITION_VECTOR_SIZE];
    double r_real[POSITION_VECTOR_SIZE];

    zeno.getFeetPositions (
            l_expected,
            r_expected,
            l_real,
            r_real);

    fprintf (FFeetLog, "%f %f %f    %f %f %f", 
            l_expected[0], l_expected[1], l_expected[2], 
            l_real[0], l_real[1], l_real[2]);
    fprintf (FFeetLog, "     %f %f %f    %f %f %f ;\n", 
            r_expected[0], r_expected[1], r_expected[2],
            r_real[0], r_real[1], r_real[2]);
}


void oruw_log::logSolverInfo (smpc::solver *solver, int mpc_solver_type)
{
    if (solver != NULL)
    {
        if (mpc_solver_type == SOLVER_TYPE_AS)
        {
            smpc::solver_as * solver_ptr = dynamic_cast<smpc::solver_as *>(solver);
            if (solver_ptr != NULL)
            {
                fprintf(oruw_log_instance->FMessages, "AS size = %i // Added = %i // Removed = %i\n",
                        solver_ptr->active_set_size,
                        solver_ptr->added_constraints_num,
                        solver_ptr->removed_constraints_num);
            }
        }
        else if (mpc_solver_type == SOLVER_TYPE_IP)
        {
            smpc::solver_ip * solver_ptr = dynamic_cast<smpc::solver_ip *>(solver);
            if (solver_ptr != NULL)
            {
                fprintf(oruw_log_instance->FMessages, "ext loops = %d // int loops = %d // bs loops = %d\n",
                        solver_ptr->ext_loop_iterations,
                        solver_ptr->int_loop_iterations,
                        solver_ptr->bt_search_iterations);
            }
        }
    }
}
#endif // ORUW_LOG_ENABLE