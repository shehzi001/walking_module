/**
 * @file
 * @author Alexander Sherikov
 * @mainainter Shehzad Ahmed
 */

#include "walk_parameters.h"
#include "smpc_solver.h"
#include <iostream>



/**
 * @brief Initialize parameters to default values.
 *
 */
walkParameters::walkParameters()
{
}

void walkParameters::walkParametersInitialize()
{
    /* 
     * Feedback gains for com feedback controller
    */
    feedback_threshold_x = 0.02;
    feedback_threshold_y = 0.02;
    feedback_gain_x = 0.01;
    feedback_gain_y = 0.01;

    /**
     * Feedforward CoM threshold limiter parameters
     */
    activate_com_limiter = true;
    feedforward_limit_y = 0.03;
    feedforward_limit_gain = 0.02;

    controller_type_ = COM_TRACKER;

    com_offset_ = 0.002;

    // parameters of the MPC solver
    mpc_solver_type = SOLVER_TYPE_AS;

    if (controller_type_ == WAIST_TRACKER)
        mpc_gain_position = 150.0;   // closeness to the reference ZMP points
    else if (controller_type_ == COM_TRACKER)
        mpc_gain_position = 50.0;
    else
        mpc_gain_position = 50.0;
    mpc_gain_acceleration = 0.02; // penalty for the acceleration
    mpc_gain_velocity = 1.0;      // penalty for the velocity
    mpc_gain_jerk = 1.0;          // penalty for the jerk

    mpc_as_tolerance = 1e-7;
    mpc_as_max_activate = 5;
    mpc_as_use_downdate = true;
    
    mpc_ip_tolerance_int = 1e-1;
    mpc_ip_tolerance_ext = 1e+4;
    mpc_ip_t = 1e-1;
    mpc_ip_mu = 1.0;
    mpc_ip_bs_alpha = 0.01;
    mpc_ip_bs_beta = 0.9;
    mpc_ip_max_iter = 5;
    mpc_ip_bs_type = smpc::SMPC_IP_BS_LOGBAR;


    /**
     * parameters of the walking pattern generator
     */
    preview_window_size = 20;
    preview_sampling_time_ms = 80.0;
    preview_sampling_time_sec = (double) preview_sampling_time_ms / 1000;

    ss_time_ms = 400.0;
    ds_time_ms = 40.0;
    ds_number = 3;
    step_pairs_number = 4;


    // parameters of the steps
    /**
     * @ref AldNaoPaper "0.015 in the publication by Aldebaran-Robotics"
     * 0.02 is used in the built-in module, but is is slightly higher when 
     * executed on the robot.
     */
    step_height = 0.02;

    // The longest step in the built-in module.
    step_length = 0.04;
    com_max_height = 0.0;
    enable_variable_com = true;

    // Refer to smpc_solver/include/WMG.h for the description of these  parameters.
    bezier_weight_1 = 1.5;
    bezier_weight_2 = 3.0;
    bezier_inclination_1 = 0.015;
    bezier_inclination_2 = 0.01;

    // assume that z coordinate of a foot after position correction is 0.0
    set_support_z_to_zero = true;


    // parameters of the control thread    
    /*
     * Accordingly to the NAOqi documentation
     * @verbatim
        Only DCM has a real-time thread in NAOqi. ::
            scheduling policy: SCHED_FIFO
            scheduling priority: 90
       @endverbatim
     * Cannot set priority >= 70.
     */
    walk_control_thread_priority = 65; // constant

    dcm_time_shift_ms = 0.0;
    dcm_sampling_time_ms = 10.0; // constant

    control_loop_time_ms = 40.0; // constant
    control_sampling_time_ms = 40.0; // constant
    control_sampling_time_sec = (double) control_sampling_time_ms / 1000;

    loop_time_limit_ms = 15; // less than control_sampling_time_ms


    walk_pattern = WALK_PATTERN_STRAIGHT;
    walk_pattern_initial_support_foot = 1;
    walk_pattern_diagonal_shift = -0.01;
    walk_pattern_circular_radius = 0.1;

    footstep_planner_type = INTERNAL;

    activate_arm_swing = false;
    arm_swing_min = 0.8;
    arm_swing_max = 1.4;

    activate_state_feedback_controller = true;


    // IGM    
    // Acc. to the reference: "32 x Hall effect sensors. 12 bit precision, ie 4096 values per turn"
    // i.e. 3.14*2/4096 = 0.0015 radian is the lowest detectable change in a joint angle.
    igm_tol = 0.0051;//(0.29 degrees); //0.088; 
    igm_max_iter = 100;
    igm_mu = 1.2;

    igm_type = CONSTRAIN_WAIST_PITCH;
    igm_waist_pitch_constraint_angle = 0.08;
    igm_waist_pitch_samples = 6;
    igm_waist_pitch_sampling_step = 0.01;

    // logging
    activate_logging  =  true;
    logger_file_path = "./";
    logger_file_name = "zeno_test_run.m";
    logger_file_full_loc = logger_file_path + logger_file_name;


    // simulation
    simulation = false;
    screenWidth=800;
    screenHeight=600;

    // initial view
    zoom = 10.0;
    rotx = -65.0;
    roty = 0.0;
    rotz = 0.0;
    transx = 0.0;
    transy = 0.0;

    distance_x1 = 0.105;
    distance_x2 = 0.08;
    distance_y1 = 0.0425;
    distance_y2 = 0.0425;
    support_distance_y = 0.095;

    // list of parameters, that are stored in config file:

    //param_names.arraySetSize(NUM_PARAMETERS);

    param_names[FEEDBACK_GAIN]            = "feedback_gain";
    param_names[FEEDBACK_THRESHOLD]       = "feedback_threshold";

    param_names[MPC_SOLVER_TYPE]          = "mpc_solver_type";

    param_names[MPC_GAIN_VELOCITY]        = "mpc_gain_velocity";
    param_names[MPC_GAIN_POSITION]        = "mpc_gain_position";
    param_names[MPC_GAIN_JERK]            = "mpc_gain_jerk";
    param_names[MPC_GAIN_ACCELERATION]    = "mpc_gain_acceleration";

    param_names[MPC_AS_TOLERANCE]         = "mpc_as_tolerance";
    param_names[MPC_AS_MAX_ACTIVATE]      = "mpc_as_max_activate";
    param_names[MPC_AS_USE_DOWNDATE]      = "mpc_as_use_downdate";
                                             
    param_names[MPC_IP_TOLERANCE_INT]     = "mpc_ip_tolerance_int";
    param_names[MPC_IP_TOLERANCE_EXT]     = "mpc_ip_tolerance_ext";
    param_names[MPC_IP_T]                 = "mpc_ip_t";
    param_names[MPC_IP_MU]                = "mpc_ip_mu";
    param_names[MPC_IP_BS_ALPHA]          = "mpc_ip_bs_alpha";
    param_names[MPC_IP_BS_BETA]           = "mpc_ip_bs_beta";
    param_names[MPC_IP_MAX_ITER]          = "mpc_ip_max_iter";
    param_names[MPC_IP_BS_TYPE]           = "mpc_ip_bs_type";
                                            
    param_names[IGM_MU]                   = "igm_mu";

    param_names[STEP_HEIGHT]              = "step_height";
    param_names[STEP_LENGTH]              = "step_length";
    param_names[ENABLE_VARIABLE_COM]              = "enable_variable_com";
    
    param_names[BEZIER_WEIGHT_1]          = "bezier_weight_1";
    param_names[BEZIER_WEIGHT_2]          = "bezier_weight_2";
    param_names[BEZIER_INCLINATION_1]     = "bezier_inclination_1";
    param_names[BEZIER_INCLINATION_2]     = "bezier_inclination_2";

    param_names[LOOP_TIME_LIMIT_MS]       = "loop_time_limit_ms";
    param_names[DCM_TIME_SHIFT_MS]        = "dcm_time_shift_ms";
    param_names[PREVIEW_SAMPLING_TIME_MS] = "preview_sampling_time_ms";
    param_names[PREVIEW_WINDOW_SIZE]      = "preview_window_size";     

    param_names[SS_CONTROL_LOOPS]         = "ss_control_loops";     
    param_names[DS_CONTROL_LOOPS]         = "ds_control_loops";     
    param_names[DS_NUMBER]                = "ds_number";     
    param_names[STEP_PAIRS_NUMBER]        = "step_pairs_number";     

    param_names[WALK_PATTERN]             = "walk_pattern";

    param_names[FOOTSTEP_PLANNER_TYPE]    = "footstep_planner_type";
}

void walkParameters::walkParametersFromROSServer(const ros::NodeHandle &nh)
{
    walkParametersInitialize();
    dcm_time_shift_ms = 0;
    dcm_sampling_time_ms = 10; // constant

    nh.param<double>("parameters_walk_controller/control_loop_time_limit_ms", loop_time_limit_ms, 15);
    nh.param<double>("parameters_walk_controller/control_loop_time_ms", control_loop_time_ms, 40);
    nh.param<double>("parameters_walk_controller/control_sampling_time_ms", control_sampling_time_ms, 40);
    control_sampling_time_sec = (double) control_sampling_time_ms / 1000;

    nh.param<double>("parameters_walk_controller/com_offset", com_offset_, 0.002);
    nh.param<int>("parameters_walk_controller/controller_type", controller_type_, 1);
    nh.param<bool>("parameters_walk_controller/activate_arm_swing", activate_arm_swing, false);
    nh.param<double>("parameters_walk_controller/arm_swing_min", arm_swing_min, 0.8);
    nh.param<double>("parameters_walk_controller/arm_swing_max", arm_swing_max, 1.4);

    // parameters of the Walk pattern generator
    nh.param<int>("parameters_walk_pattern_generator/preview_window_size", preview_window_size, 20);
    nh.param<double>("parameters_walk_pattern_generator/preview_sampling_time_ms", preview_sampling_time_ms, 80);
    preview_sampling_time_sec = (double) preview_sampling_time_ms / 1000;
    
    nh.param<double>("parameters_walk_pattern_generator/ss_time_ms", ss_time_ms, 800);
    nh.param<double>("parameters_walk_pattern_generator/ds_time_ms", ds_time_ms, 80);
    nh.param<int>("parameters_walk_pattern_generator/ds_number", ds_number, 3);
    nh.param<int>("parameters_walk_pattern_generator/step_pairs_number", step_pairs_number, 4);

    nh.param<double>("parameters_walk_pattern_generator/step_height", step_height, 0.02);
    nh.param<double>("parameters_walk_pattern_generator/step_length", step_length, 0.04);
    nh.param<double>("parameters_walk_pattern_generator/com_max_height", com_max_height, 0.0);
    nh.param<bool>("parameters_walk_pattern_generator/enable_variable_com_height", enable_variable_com, true);
    

    nh.param<double>("parameters_walk_pattern_generator/bezier_inclination_1", bezier_inclination_1, 0.015);
    nh.param<double>("parameters_walk_pattern_generator/bezier_inclination_2", bezier_inclination_2, -0.01);
    nh.param<double>("parameters_walk_pattern_generator/bezier_weight_1", bezier_weight_1, 1.5);
    nh.param<double>("parameters_walk_pattern_generator/bezier_weight_2", bezier_weight_2, 3.0);

    nh.param<bool>("parameters_walk_pattern_generator/set_support_z_to_zero", set_support_z_to_zero, true);

    nh.param<int>("parameters_walk_pattern_generator/walk_pattern", walk_pattern, 0);
    nh.param<int>("parameters_walk_pattern_generator/walk_pattern_initial_support_foot", walk_pattern_initial_support_foot, 1);
    nh.param<double>("parameters_walk_pattern_generator/walk_pattern_diagonal_shift", walk_pattern_diagonal_shift, 0.01);
    nh.param<double>("parameters_walk_pattern_generator/walk_pattern_circular_radius", walk_pattern_circular_radius, 0.1);
    nh.param<int>("parameters_footstep_planner/footstep_planner_type", footstep_planner_type, 0);

    // parameters of the MPC solver
    nh.param<int>("parameters_mpc_solver/mpc_solver_type", mpc_solver_type, 0); //SOLVER_TYPE_AS

    if (controller_type_ == WAIST_TRACKER)
        nh.param<double>("parameters_mpc_solver/waist_tracker/mpc_gain_position", 
                                        mpc_gain_position, 150.0);
    else if (controller_type_ == COM_TRACKER)
        nh.param<double>("parameters_mpc_solver/com_tracker/mpc_gain_position", 
                                        mpc_gain_position, 50.0);
    else
        nh.param<double>("parameters_mpc_solver/mpc_gain_position", 
                                        mpc_gain_position, 50.0);

    nh.param<double>("parameters_mpc_solver/mpc_gain_acceleration", 
                                        mpc_gain_acceleration, 0.02); // penalty for the acceleration
    nh.param<double>("parameters_mpc_solver/mpc_gain_velocity", 
                                        mpc_gain_velocity, 1.0);  // penalty for the velocity
    nh.param<double>("parameters_mpc_solver/mpc_gain_jerk", 
                                        mpc_gain_jerk, 1.0); // penalty for the jerk

    nh.param<double>("parameters_mpc_solver/mpc_as_solver/mpc_as_tolerance", 
                                        mpc_as_tolerance, 1e-7); 
    nh.param<int>("parameters_mpc_solver/mpc_as_solver/mpc_as_max_activate", 
                                        mpc_as_max_activate, 5);  
    nh.param<bool>("parameters_mpc_solver/mpc_as_solver/mpc_as_use_downdate", 
                                        mpc_as_use_downdate, true);

    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_tolerance_int",
                                        mpc_ip_tolerance_int, 1e-1);
    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_tolerance_ext",
                                        mpc_ip_tolerance_ext, 1e+4);
    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_t",
                                        mpc_ip_t, 1e-1);
    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_mu",
                                        mpc_ip_mu, 1.0);
    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_bs_alpha",
                                        mpc_ip_bs_alpha, 0.01);
    nh.param<double>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_bs_beta",
                                        mpc_ip_bs_beta, 0.9);
    nh.param<int>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_max_iter",
                                        mpc_ip_max_iter, 5);
    nh.param<int>("parameters_mpc_solver/mpc_ip_solver/mpc_ip_bs_type",
                                        mpc_ip_bs_type, 1); //smpc::SMPC_IP_BS_LOGBAR

    // parameters of the igm
    nh.param<int>("parameters_igm/igm_max_iter", igm_max_iter, 100);
    nh.param<double>("parameters_igm/igm_mu", igm_mu, 1.2);
    nh.param<double>("parameters_igm/igm_tol", igm_tol, 0.0051);
    nh.param<int>("parameters_igm/igm_type", igm_type, 1);
    nh.param<double>("parameters_igm/igm_waist_pitch_constraint_angle",
                                            igm_waist_pitch_constraint_angle, 0.08);
    nh.param<int>("parameters_igm/igm_waist_pitch_samples",
                                            igm_waist_pitch_samples, 6);
    nh.param<double>("parameters_igm/igm_waist_pitch_sampling_step",
                                            igm_waist_pitch_sampling_step, 0.01);

    //Feedback gains for com feedback controller
    nh.param<double>("parameters_feedback_controllers/state_feedback_controller/feedback_gain_x",
                                        feedback_gain_x, 0.01);
    nh.param<double>("parameters_feedback_controllers/state_feedback_controller/feedback_gain_y",
                                        feedback_gain_y, 0.12);
    nh.param<double>("parameters_feedback_controllers/state_feedback_controller/feedback_threshold_x",
                                        feedback_threshold_x, 0.02);
    nh.param<double>("parameters_feedback_controllers/state_feedback_controller/feedback_threshold_y",
                                        feedback_threshold_y, 0.02);
    nh.param<bool>("parameters_feedback_controllers/state_feedback_controller/activate",
                                        activate_state_feedback_controller, true);

    // feedforward CoM limiter parameters
    nh.param<double>("parameters_feedback_controllers/feedforward_com_limiter/feedforward_limit_y",
                                        feedforward_limit_y, feedforward_limit_y);
    nh.param<double>("parameters_feedback_controllers/feedforward_com_limiter/feedforward_limit_gain",
                                        feedforward_limit_gain, feedforward_limit_gain);
    nh.param<bool>("parameters_feedback_controllers/feedforward_com_limiter/activate",
                                        activate_com_limiter, activate_com_limiter);

    nh.param<bool>("parameters_logger/activate_logging", activate_logging, true);
    nh.param<string>("parameters_logger/logger_file_path", logger_file_path, "./");
    nh.param<string>("parameters_logger/logger_file_name", logger_file_name, "zeno_test_run.m");
    nh.param<string>("parameters_logger/test_case_name",  logger_test_case_name, "");
    //test_case_name += logger_test_case_name + std::to_string(rand());
    //logger_file_full_loc = logger_file_path + test_case_name + "_" + logger_file_name;

    nh.param<bool>("parameters_simulation/simulation",  simulation, false);
    nh.param<int>("parameters_simulation/screenWidth",  screenWidth, 800);
    nh.param<int>("parameters_simulation/screenHeight",  screenHeight, 600);
    nh.param<float>("parameters_simulation/zoom",  zoom, 10.0);
    nh.param<float>("parameters_simulation/rotx",  rotx, -65.0);
    nh.param<float>("parameters_simulation/roty",  roty, 0.0);
    nh.param<float>("parameters_simulation/rotz",  rotz, 0.0);
    nh.param<float>("parameters_simulation/transx",  transx, 0.0);
    nh.param<float>("parameters_simulation/transy",  transy, 0.0);

    nh.param<float>("paramters_footsize/distance_x1",  distance_x1, 0.105);
    nh.param<float>("paramters_footsize/distance_x2",  distance_x2, 0.08);
    nh.param<float>("paramters_footsize/distance_y1",  distance_y1, 0.0425);
    nh.param<float>("paramters_footsize/distance_y2",  distance_y2, 0.0425);
    nh.param<float>("paramters_footsize/support_distance_y",  support_distance_y, 0.095);
}
