/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */


#include "oru_walk.h"


/**
 * @brief Update joint angles.
 */
void oru_walk::readSensors(jointState& joint_state)
{
    vector<double> sensorValues, actual_angles;

    robot_interface_->readJointPositions(joint_names, actual_angles);

    for (int i = 0; i < JOINTS_NUM; i++)
    {
        joint_state.q[i] = actual_angles[i];
    }
}

void oru_walk::readMotorState(jointState& joint_state)
{
    vector<double> sensorValues, actual_angles;

    robot_interface_->readMotorPositions(joint_names, actual_angles);

    for (int i = 0; i < JOINTS_NUM; i++)
    {
        joint_state.q[i] = actual_angles[i];
    }
}

/**
 * @brief Correct state and the model based on the sensor data.
 *
 * @param[in,out] init_state expected state
 */
void oru_walk::feedbackError (smpc::state_com &init_state)
{
    if (wp.controller_type_ == WAIST_TRACKER)
        zeno.getWaistCoM (zeno.state_sensor, zeno.CoM_position);
    else if(wp.controller_type_ == COM_TRACKER)
        zeno.getCoM (zeno.state_sensor, zeno.CoM_position);

    if(wp.activate_logging) {
        log->addRobotCoM(zeno.CoM_position[0],zeno.CoM_position[1],0);
        log->addRobotCoM(init_state.x(),init_state.y(),1);
    }

    smpc::state_com state_error;
    state_error.set (
            init_state.x() - zeno.CoM_position[0],
            init_state.y() - zeno.CoM_position[1]);

    if (state_error.x() > wp.feedback_threshold_x)
    {
        state_error.x() -= wp.feedback_threshold_x;
    }
    else if (state_error.x() < -wp.feedback_threshold_x)
    {
        state_error.x() +=wp.feedback_threshold_x;
    }
    else
    {
        state_error.x() = 0.0;
    }
    init_state.x() -= wp.feedback_gain_x * state_error.x();
    
    /*if (state_error.y() > wp.feedback_threshold_y)
    {
        state_error.y() -= wp.feedback_threshold_y;
    }
    else if (state_error.y() < -wp.feedback_threshold_y)
    {
        state_error.y() += wp.feedback_threshold_y;
    }
    else
    {
        state_error.y() = 0.0;
    }*/

    limitFeedForwardCOM(init_state);
    //init_state.y() -= wp.feedback_gain_y * state_error.y();

    if(wp.activate_logging) {
        log->addStateError(state_error.x(), state_error.y());
    }
}

void oru_walk::limitFeedForwardCOM(smpc::state_com &com_wrt_world)
{
    if (!wp.activate_com_limiter)
        return;

    double com_wrt_support_foot_max_y = wp.feedforward_limit_y;

    std::vector<double> support_foot_wrt_world;
    if (zeno.support_foot == IGM_SUPPORT_LEFT) {
        support_foot_wrt_world.push_back(zeno.left_foot_posture.data()[12]);
        support_foot_wrt_world.push_back(zeno.left_foot_posture.data()[13]);
    } else if (zeno.support_foot == IGM_SUPPORT_RIGHT) {
        support_foot_wrt_world.push_back(zeno.right_foot_posture.data()[12]);
        support_foot_wrt_world.push_back(zeno.right_foot_posture.data()[13]);
    }

    double com_wrt_support_foot_x = support_foot_wrt_world[0] - com_wrt_world.x();
    double com_wrt_support_foot_y = support_foot_wrt_world[1] - com_wrt_world.y();

    double error_y = 0.0;

    /*if (com_wrt_support_foot_y >= 0) {
        if (com_wrt_support_foot_y <= com_wrt_support_foot_max_y+0.003) {
            ROS_INFO_STREAM("Lateral compensation is activated(max)." << com_wrt_support_foot_y);
            error_y = com_wrt_support_foot_max_y - com_wrt_support_foot_y;
            com_wrt_world.y() -=  wp.feedforward_limit_gain*error_y;
        }
    } else {
        if (com_wrt_support_foot_y >= -com_wrt_support_foot_max_y) {
            ROS_INFO_STREAM("Lateral compensation is activated(min)." << com_wrt_support_foot_y);
            error_y = (-com_wrt_support_foot_max_y) - com_wrt_support_foot_y;
            com_wrt_world.y() -=  wp.feedforward_limit_gain*error_y;
        }
    }*/

    if (com_wrt_support_foot_y >= 0) {
        double safe_distance_to_sf = support_foot_wrt_world[1] - (com_wrt_support_foot_max_y - 0.006);//- 0.012
        ROS_INFO_STREAM("Lateral compensation is activated(max):" << safe_distance_to_sf << " , " << com_wrt_world.y());
        if(com_wrt_world.y() >= safe_distance_to_sf) {
            com_wrt_world.y() = safe_distance_to_sf;
        }
        /*if (com_wrt_support_foot_y <= com_wrt_support_foot_max_y+0.003) {
            ROS_INFO_STREAM("Lateral compensation is activated(max)." << com_wrt_support_foot_y);
            error_y = com_wrt_support_foot_max_y - com_wrt_support_foot_y;
            com_wrt_world.y() -=  wp.feedforward_limit_gain*error_y;
        }*/
    } else {
        double safe_distance_to_sf = support_foot_wrt_world[1] + com_wrt_support_foot_max_y;
        ROS_INFO_STREAM("Lateral compensation is activated(min):" << safe_distance_to_sf << " , " << com_wrt_world.y());
        if(com_wrt_world.y() <= safe_distance_to_sf) {
            com_wrt_world.y() = safe_distance_to_sf;
        }
        /*if (com_wrt_support_foot_y >= -com_wrt_support_foot_max_y) {
            ROS_INFO_STREAM("Lateral compensation is activated(min)." << com_wrt_support_foot_y);
            error_y = (-com_wrt_support_foot_max_y) - com_wrt_support_foot_y;
            com_wrt_world.y() -=  wp.feedforward_limit_gain*error_y;
        }*/
    }

    if(wp.activate_logging) {
        if (com_wrt_support_foot_y>0) {
            log->addTorsoOrientation(support_foot_wrt_world[1], com_wrt_world.y(), 
                                                            com_wrt_support_foot_y, 0);
            log->addTorsoOrientation(error_y, com_wrt_support_foot_max_y , 0.0, 4);
        } else {
            log->addTorsoOrientation(support_foot_wrt_world[1], com_wrt_world.y(), 
                                                            com_wrt_support_foot_y, 0);
            log->addTorsoOrientation(error_y, -com_wrt_support_foot_max_y , 0.0, 4);
        }
    }
}

void oru_walk::feedbackOrientationError(smpc::state_com &init_state)
{
    double com_threshold_roll = 0.020;
    double waist_roll_threshold = 0.13;
    double Kp = 0.12;

    std::vector<double> waist_orientation;
    getWaistOrientationFromRobotModel(waist_orientation);

    std::vector<double> imu_data_temp;
    robot_interface_->getTorsoFeedback(imu_data_temp);

    double roll_error = -waist_orientation[0];
    double pitch_error = -waist_orientation[1];

    /*if(init_state.y() < -com_threshold_roll)
        init_state.y() -= Kp * -com_threshold_roll;
    else if(init_state.y() > com_threshold_roll)
        init_state.y() -=  Kp * com_threshold_roll;*/

    if(wp.activate_logging) {
        log->addTorsoOrientation(waist_orientation[0], waist_orientation[1], waist_orientation[2], 0);
        log->addTorsoOrientation(imu_data_temp[0], imu_data_temp[1], imu_data_temp[2], 2);
        log->addTorsoOrientation(roll_error, pitch_error, 0.0, 4);
    }
}

/*
void oru_walk::applyAngularVelocityControls(smpc::state_com &init_state)
{
    double threshold_roll_min = -0.008;
    double threshold_roll_max = 0.008;

    double P = 0.45;

    std::vector<double> imu_data_temp;
    robot_interface_->getTorsoFeedback(imu_data_temp);

    log->addTorsoOrientation(imu_data_temp[0], imu_data_temp[1], imu_data_temp[2], 2);

    double roll_error = -imu_data_temp[0];
    double pitch_error = -imu_data_temp[1];

    double Yam = P * roll_error;

    if (abs(roll_error) < 0.15 )
        Yam = 0.0;
    else {
        if (Yam > 0.005)
        {
            Yam = 0.001;
        } else if (Yam < -0.005){
            Yam = -0.001;
        }
    }

    log->addTorsoOrientation(roll_error, pitch_error, 0.0, 4);
    log->addAngularMomentumControls(Xam, Yam);
    
    //cout << "Momentum controller(Xam, Yam): " << Xam <<  "," << Yam << "," << endl;

    //init_state.y() += Yam;
    //init_state.x() -= Xam;
}
*/