/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 */

#include "oru_walk.h"
//#include "oruw_log.h"
//#include "oruw_timer.h"


/**
 * @brief Initialize selected walk pattern
 */
void oru_walk::initWalkPattern(WMG &wmg)
{
    if(wp.walk_pattern_initial_support_foot == 1) { // 1 = Left support foot
        // support foot position and orientation
        zeno.init (
                IGM_SUPPORT_LEFT,
                0.0, 0.044810, 0.0, // position
                0.0, 0.0, 0.0);  // orientation
        // swing foot position
        zeno.getSwingFootPosture (zeno.state_sensor, zeno.right_foot_posture.data());
    } else {
        // support foot position and orientation
        zeno.init (
                IGM_SUPPORT_RIGHT,
                0.0, -0.044810, 0.0, // position
                0.0, 0.0, 0.0);  // orientation

        // swing foot position
        zeno.getSwingFootPosture (zeno.state_sensor, zeno.left_foot_posture.data());
    }

    switch (wp.walk_pattern)
    {
        case WALK_PATTERN_STRAIGHT:
            initWalkPattern_Straight(wmg);
            break;
        case WALK_PATTERN_DIAGONAL:
            initWalkPattern_Diagonal(wmg);
            break;
        case WALK_PATTERN_CIRCULAR:
            initWalkPattern_Circular(wmg);
            break;
        case WALK_PATTERN_BACK:
            initWalkPattern_Back(wmg);
            break;
        default:
            ROS_ERROR("Unknown walk pattern.");
            break;
    }
}

/**
 * @brief Initializes walk pattern
 */
void oru_walk::initWalkPattern_Straight(WMG &wmg)
{
    // each step is defined relatively to the previous step
    const double step_x = wp.step_length;                        // relative X position
    const double step_y = wmg.def_constraints.support_distance_y;// relative Y position

    if(wp.walk_pattern_initial_support_foot == 1) { // 1 = Left support foot
        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

        // Initial double support
        wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


        // all subsequent steps have normal feet size
        wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0   , -step_y/2, 0.0);

        wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
        wmg.addFootstep(step_x,  step_y, 0.0);

        for (int i = 0; i < wp.step_pairs_number; i++)
        {
            wmg.addFootstep(step_x, -step_y, 0.0);
            wmg.addFootstep(step_x,  step_y, 0.0);
        }

        wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0   , (-step_y/2), 0.0, FS_TYPE_DS);
        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstep(0.0   , (-step_y/2), 0.0, FS_TYPE_SS_R);
    } else {
        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_SS_R);

        // Initial double support
        wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_DS);


        // all subsequent steps have normal feet size
        wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0   , step_y/2, 0.0);

        wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
        wmg.addFootstep(step_x,  -step_y, 0.0);

        for (int i = 0; i < wp.step_pairs_number; i++)
        {
            wmg.addFootstep(step_x, step_y, 0.0);
            wmg.addFootstep(step_x, -step_y, 0.0);
        }

        wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
        wmg.addFootstep(0.0   , (step_y/2), 0.0, FS_TYPE_DS);
        wmg.setFootstepParametersMS (0, 0, 0);
        wmg.addFootstep(0.0   , (step_y/2), 0.0, FS_TYPE_SS_L);

    }
}

/**
 * @brief Initializes walk pattern
 */
void oru_walk::initWalkPattern_Diagonal(WMG &wmg)
{
    // each step is defined relatively to the previous step
    const double step_x = wp.step_length;                        // relative X position
    const double step_y = wmg.def_constraints.support_distance_y;// relative Y position


    wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

    // Initial double support
    wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);

    // each step is defined relatively to the previous step
    const double shift = wp.walk_pattern_diagonal_shift;

    // all subsequent steps have normal feet size
    wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
    wmg.addFootstep(0.0   , -step_y/2, 0.0);
    wmg.addFootstep(step_x,  step_y + shift, 0.0);

    for (int i = 0; i < wp.step_pairs_number; i++)
    {
        wmg.addFootstep(step_x, -step_y + shift, 0.0);
        wmg.addFootstep(step_x,  step_y + shift, 0.0);
    }


    // here we give many reference points, since otherwise we 
    // would not have enough steps in preview window to reach 
    // the last footsteps
    wmg.setFootstepParametersMS (6*wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
    wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
}

/**
 * @brief Initializes walk pattern
 */
void oru_walk::initWalkPattern_Back(WMG &wmg)
{
    // each step is defined relatively to the previous step
    const double step_x = wp.step_length;                        // relative X position
    const double step_y = wmg.def_constraints.support_distance_y;// relative Y position


    wmg.setFootstepParametersMS(0, 0, 0);
    wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

    // Initial double support
    wmg.setFootstepParametersMS(3*wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


    // all subsequent steps have normal feet size
    wmg.setFootstepParametersMS(wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0);
    wmg.setFootstepParametersMS(wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
    wmg.addFootstep(-step_x,  step_y,   0.0);

    for (int i = 0; i < wp.step_pairs_number; i++)
    {
        wmg.addFootstep(-step_x, -step_y, 0.0);
        wmg.addFootstep(-step_x,  step_y, 0.0);
    }

    // here we give many reference points, since otherwise we 
    // would not have enough steps in preview window to reach 
    // the last footsteps
    wmg.setFootstepParametersMS(5*wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
    wmg.setFootstepParametersMS(0, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
}


/**
 * @brief Initializes walk pattern
 */
void oru_walk::initWalkPattern_Circular(WMG &wmg)
{
    // each step is defined relatively to the previous step
    const double step_x_ext = wp.step_length;                    // relative X position
    const double step_y = wmg.def_constraints.support_distance_y;// relative Y position

    const double R_ext = wp.walk_pattern_circular_radius;
    const double R_int = R_ext - step_y;

    // relative angle
    double a = asin (step_x_ext / R_ext);
    double step_x_int = step_x_ext * R_int / R_ext;


    wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

    // Initial double support
    wmg.setFootstepParametersMS (3*wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);



    wmg.setFootstepParametersMS (wp.ss_time_ms, wp.ds_time_ms, wp.ds_number);
    wmg.addFootstep(0.0   ,     -step_y/2, 0.0);
    wmg.addFootstep(step_x_int,  step_y, a);

    for (int i = 0; i < wp.step_pairs_number; i++)
    {
        wmg.addFootstep(step_x_ext, -step_y, a);
        wmg.addFootstep(step_x_int,  step_y, a);
    }

    // here we give many reference points, since otherwise we 
    // would not have enough steps in preview window to reach 
    // the last footsteps
    wmg.setFootstepParametersMS (wp.ss_time_ms, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
    wmg.setFootstepParametersMS (0, 0, 0);
    wmg.addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
}
