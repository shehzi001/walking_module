/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @maintainer Shehzad Ahmed
 */


#ifndef ORU_WALK_H
#define ORU_WALK_H


//----------------------------------------
// INCLUDES
//----------------------------------------

// standard headers
#include <string> // string

// other libraries
#include <boost/bind.hpp> // callback hook
#include <boost/thread.hpp>
#include <boost/function.hpp>

// our headers
#include "WMG.h" // footsteps & parameters
#include "smpc_solver.h" // solver
#include "joints_sensors_id.h"
#include "zeno_igm.h"
#include "walk_parameters.h"

#include <robot_controller_interface.h>

 #include <stdio.h>
#include <Eigen/Core>
#include <Eigen/Cholesky>
 #include "oru_log.h"

#include <chrono>


#include <GL/gl.h>
#include <GL/glu.h>


#include <ros/ros.h>

#include "footstep.h"
#include <humanoid_nav_msgs/StepTargetArray.h>
#include <std_msgs/String.h>

using ns = chrono::nanoseconds;
using get_time = chrono::steady_clock;

#define PI 3.14156

//----------------------------------------
// DEFINITIONS
//----------------------------------------

using namespace std;

/**
 * @brief The main walking module class.
 */
class oru_walk
{
    public:
        // constructors / destructors
        oru_walk(const string &name, const ros::NodeHandle &nh);
        ~oru_walk ();

        // is called automatically when a library is loaded 
        void init();
        void initRobot();
        void setJointStiffness(bool stiffness);

        // These methods will be advertised to other modules.
        void dcmCallback();
        bool dcmCallbackwait();

        bool initwalkControl();
        void walkControl();
        void postureController();

        void executeCycle();

        void stepsCallBack(const humanoid_nav_msgs::StepTargetArray::ConstPtr& msg){
            step_target_array = *msg;
            is_step_command_recieved_ = true;
        }

        void eventInCallBack(const std_msgs::String::ConstPtr& msg){
            is_step_command_recieved_ = true;
        }


        /**
         * simulation funtions
         */
         void initialize_simulation(WMG &wmg);
         void simulate();


        /** \brief SDL pre-draw function.

            \return void
        */
        void pre_draw();


        /** \brief SDL events handling.

            \return void
        */
        void events();


        /** \brief SDL related initializations.

            \return void
        */
        void initSDL();



        /** \brief Mouse and keyboard interaction handling.

            \return void
        */
        void tick();


        /** \brief Draws a right-handed coordinate frame in OpenGL.

            \return void
        */
        void draw_CF(void);


        // The end-effectors for the feet are not in the center of the support polygons.
        // I assume
        //  ------------------------------------------------
        //  |                    |                         |
        //  |                    |0.025                    |
        //  |                    |                         |
        //  |      0.03          |          0.09           |
        //  |------------------- 0 ------------------------|
        //  |                    |                         |
        //  |                    |                         |
        //  |                    |0.025                    |
        //  |                    |                         |
        //  ------------------------------------------------
        //
        void drawPolygon();


        /** \brief Draws a cube in OpenGL.

            \return void
        */
        void drawCube();

        /** \brief Draws a grid in OpenGL.

            \param[in] grid_size Size of the grid in number of squares

            \return void
        */
        void draw_grid(int grid_size);



        /** \brief 4x4 homogeneous matrix (double T[16]) to 4x4 homogeneous matrix (GLfloat T[16])

            \return void
         */
        void HomogGL(double *T, GLfloat *M, double scale);




        /** \brief Creates a 4x4 homogeneous matrix of GLfloat (and possibly scales the position part)

            \return void
         */
        void HomogGL(double x, double y , double z, double alpha, 
                                double beta, double gamma, GLfloat *M, double scale);



        // structure of array A
        // i[4*4]      - joint i (i=1..26)
        // (26+i)[4*4] - end-effector i (i=i..7)
        // 34[3]       - CoM
        void draw_zeno(int case_flag, double *q, Eigen::Transform<double, 3>& support_foot_posture);



        // draw footsteps to execute
        void draw_footsteps(
                std::vector<double> & x_coord,
                std::vector<double> & y_coord,
                std::vector<double> & angle_rot);



        void drawSDL (
                int delay,
                std::vector<double> & x_coord, 
                std::vector<double> & y_coord, 
                std::vector<double> & angle_rot,
                igmSupportFoot support_foot,
                double *q,
                Eigen::Transform<double,3>& support_foot_posture);


    private:
        /**
         * Copy Ctor.
         */
        oru_walk(const oru_walk &other);

        /**
         * Assignment operator
         */
        oru_walk &operator=(const oru_walk &other);

    public:
        std::vector<string> joint_names;
        std::vector<string> walking_control_joint_names;
        std::vector<double> init_joint_angles;
        std::vector<double> init_joint_velocities;

    private:
        void initWalkCommands ();
        void initJointAngles ();

        void initWalkPattern(WMG &);
        void initWalkPattern(WMG &wmg, humanoid_nav_msgs::StepTargetArray step_array);
        void initWalkPattern_Straight_Abs(WMG &, humanoid_nav_msgs::StepTargetArray step_array);
        void initWalkPattern_Straight(WMG &);
        void initWalkPattern_Diagonal(WMG &);
        void initWalkPattern_Circular(WMG &);
        void initWalkPattern_Back(WMG &wmg);
        void initSolver();
        void sleepcp(double milliseconds);

        // walking
        void readSensors (jointState&);
        void readMotorState(jointState& joint_state);
        bool solveMPCProblem (WMG&, smpc_parameters&);
        bool solveIKsendCommands (const smpc_parameters&, const smpc::state_com &, const int, WMG&);

        void correctNextSupportPosition(WMG &);
        void feedbackError (smpc::state_com &);
        void feedbackOrientationError(smpc::state_com &init_state);
        void limitFeedForwardCOM(smpc::state_com &init_state);
        void getWaistOrientationFromRobotModel(std::vector<double> &waist_orientation);
        void postureControllerFromRobotModel(WMG &wmg);

        int* last_dcm_time_ms_ptr;

        std::vector<double> joint_commands;

        zeno_igm zeno;
        double ref_joint_angles[LOWER_JOINTS_NUM];

        walkParameters wp;
        smpc::solver *solver;

        boost::shared_ptr<RobotControllerInterface> robot_interface_;

        std::string connection_ip_;
        int connection_port_;

        double default_joint_velocities_;
        int joint_indices_[JOINTS_NUM];
        std::vector<double> torso_feedback_init_;
        //test_log log;
        boost::shared_ptr<test_log> log;

        int dcm_loop_counter;
        int last_dcm_time_ms;
        bool dcm_loop_break;

        //boost::condition_variable walk_control_condition;
        //boost::mutex walk_control_mutex;

        double avg_control_loop_time;
        long control_loop_counter;

        std::vector<double> waist_orientation_model_init_;

         /**
         * ROS node handle.
         */
        ros::NodeHandle nh_;
        humanoid_nav_msgs::StepTargetArray step_target_array;
        bool is_step_command_recieved_;
        ros::Subscriber step_command_sub_;
        ros::Subscriber event_in_sub_;


        /**
        *  arm swing motion
        */
        std::vector<double> arm_swing_motion_buffer;
        int left_arm_swing_index_;
        int right_arm_swing_index_;
        int left_arm_swing_direction_;
        int right_arm_swing_direction_;

        void init_arm_swing_motion(int N, int left_arm_direction, int right_arm_direction);
        void get_arm_swing_motion(int support_foot, double &left_arm, double &right_arm);


        /**
         * simulation
         */
        vector<double> x_coord;
        vector<double> y_coord;
        vector<double> angle_rot;
        bool is_simulation_running;

        int isRunning;

        // ----------------------
        int screenWidth;
        int screenHeight;

        // initial view
        float zoom;
        float rotx;
        float roty;
        float rotz;
        float transx;
        float transy;
};

#endif  // ORU_WALK_H
