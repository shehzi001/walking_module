/**
 * @file
 * @author Antonio Paolillo
 * @author Dimitar Dimitrov
 * @author Alexander Sherikov
 * @mainainer Shehzad Ahmed
 */


#ifndef WALK_PARAMETERS_H
#define WALK_PARAMETERS_H


//----------------------------------------
// INCLUDES
//----------------------------------------
#include <map>
#include <string>
#include <ros/ros.h>

//----------------------------------------
// DEFINITIONS
//----------------------------------------

using namespace std;

enum walkPatterns
{
    WALK_PATTERN_STRAIGHT = 0,
    WALK_PATTERN_DIAGONAL = 1,
    WALK_PATTERN_CIRCULAR = 2,
    WALK_PATTERN_BACK = 3
};

enum ControllerTypes
{
    WAIST_TRACKER         = 0 ,
    COM_TRACKER           = 1 ,
};

enum IgmTypes
{
    AUTO                        = 0 ,
    CONSTRAIN_WAIST_PITCH       = 1 ,
    CONSTRAIN_WAIST_ROLL_PITCH  = 2 ,
};

enum solverTypes
{
    SOLVER_TYPE_AS = 0,
    SOLVER_TYPE_IP = 1
};

enum footstepPlannerType
{
    INTERNAL = 0,
    EXTERNAL = 1
};

enum parametersNames
{
    FEEDBACK_GAIN               ,
    FEEDBACK_THRESHOLD          ,

    MPC_SOLVER_TYPE             ,
    MPC_GAIN_POSITION           ,
    MPC_GAIN_VELOCITY           ,
    MPC_GAIN_ACCELERATION       ,
    MPC_GAIN_JERK               ,

    MPC_AS_TOLERANCE            ,
    MPC_AS_MAX_ACTIVATE         ,
    MPC_AS_USE_DOWNDATE         ,

    MPC_IP_TOLERANCE_INT        ,
    MPC_IP_TOLERANCE_EXT        ,
    MPC_IP_T                    ,
    MPC_IP_MU                   ,
    MPC_IP_BS_ALPHA             ,
    MPC_IP_BS_BETA              ,
    MPC_IP_MAX_ITER             ,
    MPC_IP_BS_TYPE              ,

    IGM_MU                      ,

    STEP_HEIGHT                 ,
    STEP_LENGTH                 ,
    COM_MAX_HEIGHT              ,
    ENABLE_VARIABLE_COM         ,
    BEZIER_WEIGHT_1             ,
    BEZIER_WEIGHT_2             ,
    BEZIER_INCLINATION_1        ,
    BEZIER_INCLINATION_2        ,

    LOOP_TIME_LIMIT_MS          ,
    DCM_TIME_SHIFT_MS           ,
    PREVIEW_SAMPLING_TIME_MS    ,
    PREVIEW_WINDOW_SIZE         ,
    SS_CONTROL_LOOPS            ,
    DS_CONTROL_LOOPS            ,
    DS_NUMBER                   ,

    STEP_PAIRS_NUMBER           ,
    WALK_PATTERN                ,
    WALK_PATTERN_DIAGONAL_SHIFT ,
    WALK_PATTERN_CIRCULAR_RADIUS,
    FOOTSTEP_PLANNER_TYPE       ,

    NUM_PARAMETERS              
};


/**
 * @brief A container for parameters.
 */
class walkParameters
{
    public:
        walkParameters();
        void walkParametersInitialize();
        void walkParametersFromROSServer(const ros::NodeHandle &nh);

        double feedback_gain;
        double feedback_threshold;
        double feedback_threshold_x;
        double feedback_threshold_y;
        double feedback_gain_x;
        double feedback_gain_y;
        bool activate_state_feedback_controller;

        bool activate_com_limiter;
        double feedforward_limit_y;
        double feedforward_limit_gain;

        int mpc_solver_type;

        double mpc_gain_position;
        double mpc_gain_velocity;
        double mpc_gain_acceleration;
        double mpc_gain_jerk;

        double mpc_as_tolerance;
        int mpc_as_max_activate;
        bool mpc_as_use_downdate;
        
        double mpc_ip_tolerance_int;
        double mpc_ip_tolerance_ext;
        double mpc_ip_t;
        double mpc_ip_mu;
        double mpc_ip_bs_alpha;
        double mpc_ip_bs_beta;
        int mpc_ip_max_iter;
        int mpc_ip_bs_type;


        double step_height;
        double step_length;
        double com_max_height;
        bool enable_variable_com;

        int walk_control_thread_priority;

        double dcm_sampling_time_ms;
        double dcm_time_shift_ms;
        double control_loop_time_ms;
        double control_sampling_time_ms;
        double control_sampling_time_sec;
        double loop_time_limit_ms;
        double preview_sampling_time_ms;
        double preview_sampling_time_sec;
        int preview_window_size;

        bool set_support_z_to_zero;

        double ss_time_ms;
        double ds_time_ms;
        int ds_number;
        int step_pairs_number;

        int walk_pattern;
        int walk_pattern_initial_support_foot;
        double walk_pattern_diagonal_shift;
        double walk_pattern_circular_radius;
        int controller_type_;

        double com_offset_;

        double igm_tol;
        int igm_max_iter;
        double igm_mu;
        int igm_type;
        double igm_waist_pitch_constraint_angle;
        int igm_waist_pitch_samples;
        double igm_waist_pitch_sampling_step;

        double bezier_weight_1;
        double bezier_weight_2;
        double bezier_inclination_1;
        double bezier_inclination_2;

        int footstep_planner_type;

        bool activate_arm_swing;
        double arm_swing_min;
        double arm_swing_max;


        bool activate_logging;
        string logger_file_path;
        string logger_file_name;
        string logger_file_full_loc;
        string logger_test_case_name;

        std::map<int, string> param_names;

        bool simulation;
        int screenWidth;
        int screenHeight;

        float zoom;
        float rotx;
        float roty;
        float rotz;
        float transx;
        float transy;

        float distance_x1;
        float distance_x2;
        float distance_y1;
        float distance_y2;
        float support_distance_y;
};

#endif  // WALK_PARAMETERS_H
