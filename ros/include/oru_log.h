/** 
 * @file
 * @author Alexander Sherikov
 * @date 05.01.2012 17:00:43 MSK
 */



/****************************************
 * INCLUDES 
 ****************************************/



/****************************************
 * FUNCTIONS 
 ****************************************/
#include<iostream>
#include<vector>
#include<stdio.h>
#include<zeno_igm.h>

class test_log
{
    public:
        test_log(const char *filename);

        ~test_log();

        void addZMPrefPoint(const double x, const double y);
        void addZMPpoint(const double x, const double y);
        void addCoMpoint(const double x, const double y, const double z);
        void addRobotCoM(const double x, const double y, int sampling_index);
        void addStateError(const double x, const double y);
        void addTorsoOrientation(const double x, const double y, const double z, int source);
        void addFeetPositions (const zeno_igm &zeno);

        void addCoMSupportChange(const double x, const double y);
        //void addAngularMomentumControls(const double x, const double y);

        void flushLeftFoot ();
        void flushRightFoot ();
        void flushZMP ();
        void flushZMPref ();
        void flushCoM ();
        void flushRobotCoM();
        void flushStateError();
        void flushTorsoOrientation ();
        void flushCoMSupportChange ();
        //void flushAngularMomentumControls();
        void close_log();
    private:

        void printVectors (
                            FILE *file_op, 
                            std::vector<double> &data_x, 
                            std::vector<double> &data_y, 
                            const char *name,
                            const char *format);


        void printVectors (
                            FILE *file_op, 
                            std::vector<double> &data_x, 
                            std::vector<double> &data_y, 
                            std::vector<double> &data_z, 
                            const char *name,
                            const char *format);

        void printVectorsTorso (
                            FILE *file_op, 
                            std::vector<double> &data_x, 
                            std::vector<double> &data_y, 
                            std::vector<double> &data_z, 
                            const char *name,
                            const char *format);

        FILE *file_op;

        std::vector<double> ZMP_x;
        std::vector<double> ZMP_y;
        std::vector<double> ZMPref_x;
        std::vector<double> ZMPref_y;
        std::vector<double> CoM_x;
        std::vector<double> CoM_y;
        std::vector<double> CoM_z;
        std::vector<double> torso_x;
        std::vector<double> torso_y;
        std::vector<double> torso_z;
        std::vector<double> torso_err_x;
        std::vector<double> torso_err_y;
        std::vector<double> torso_err_z;
        
        std::vector<double> imu_x;
        std::vector<double> imu_y;
        std::vector<double> imu_z;
        std::vector<double> imu_err_x;
        std::vector<double> imu_err_y;
        std::vector<double> imu_err_z;

        std::vector<double> CoM_x_sc;
        std::vector<double> CoM_y_sc;
        std::vector<double> actual_CoM_x;
        std::vector<double> actual_CoM_y;
        std::vector<double> expected_CoM_x;
        std::vector<double> expected_CoM_y;

        std::vector<double> state_error_x;
        std::vector<double> state_error_y;

        std::vector<double> left_foot_x;
        std::vector<double> left_foot_y;
        std::vector<double> left_foot_z;
        std::vector<double> right_foot_x;
        std::vector<double> right_foot_y;
        std::vector<double> right_foot_z;

        //std::vector<double> angular_momentum_controls_x;
        //std::vector<double> angular_momentum_controls_y;

        bool is_logger_closed;
};
