/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface.h
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */


#ifndef ROBOT_CONTROLLER_INTERFACE_H_
#define ROBOT_CONTROLLER_INTERFACE_H_

//----------------------------------------
// INCLUDES
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <brics_actuator/JointPositions.h>
#include <brics_actuator/JointVelocities.h>
#include <brics_actuator/JointValue.h>
#include <std_msgs/Float64MultiArray.h>
 
#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

#include <iostream>
#include <vector>
#include <map>

/**
 * @brief The main class to provide interface to the robot joint controller 
    and sensor data.
 */
class RobotControllerInterface
{
    public:
        /**
         * Ctor.
         */
        RobotControllerInterface();

        /**
         * Dtor.
         */
        ~RobotControllerInterface();


        /**
         * Initializes robot controller interface
         */
        void initialize(const ros::NodeHandle &nh);

        /**
         * Sends position commands to joint position controller.
         */
        void sendPositionCommand(const std::vector<std::string> &joint_names, 
                                    const std::vector<double> &joint_positions);

        /**
         * Sends velocity commands to joint position controller.
         */
        void sendVelocityCommand(const std::vector<std::string> &joint_names,
                                    const std::vector<double> &joint_velocities);

        /**
         * Reads current joint positions.
         */
        bool readJointPositions(const std::vector<std::string> &joint_names, 
                                    std::vector<double> &joint_positions);

        bool readMotorPositions(const std::vector<std::string> &joint_names, 
                                    std::vector<double> &joint_positions);

        /**
         * Reads current torso orientation feedback.
         */
        bool getTorsoFeedback(std::vector<double> &torso_feedback);

        /**
         *
         */
        void onJointStatesReceive(boost::function< void() > callbackFunc);

        /**
         * Check if joint position command is reached.
         */
        bool isJointCommandReached(const std::vector<std::string> &joint_names, 
                                    std::vector<double> &joint_positions);

    private:
        /**
         * Callback for joint state
         */
        void jointStatescallback(const sensor_msgs::JointState::ConstPtr &msg);
        void motorStatescallback(const sensor_msgs::JointState::ConstPtr &msg);

        /**
         * Callback to store torso state
         */
        void torsoStatecallback(const std_msgs::Float64MultiArray::ConstPtr &msg);

        /**
         * Check if the error between the current and commanded joint
         * is within threshold limit.
         */
        bool checkSetPointThreshold(double current_value, double command_value);

    private:
        /**
         * Copy Ctor.
         */
        RobotControllerInterface(const RobotControllerInterface &other);

        /**
         * Assignment operator
         */
        RobotControllerInterface &operator=(const RobotControllerInterface &other);

    private:
        /**
         * ROS node handle.
         */
        //ros::NodeHandle nh_;

        /**
         * Subscribers.
         */
        ros::Subscriber joint_states_sub_;
        ros::Subscriber motor_states_sub_;
        ros::Subscriber torso_state_sub_;

        /**
         * Publishers.
         */
        ros::Publisher joint_position_command_pub_;
        ros::Publisher joint_velocity_command_pub_;

        /**
         * Stores joint states.
         */
        std::map<std::string, double> joint_states_map_;
        std::map<std::string, double> motor_states_map_;

        /**
         * Stores torso state.
         */
        std::vector<double> torso_state_;

        boost::function<void(void)> on_joint_states_callback_;

        double joint_angle_threshold_;
};

#endif  // ROBOT_CONTROLLER_INTERFACE_H_
