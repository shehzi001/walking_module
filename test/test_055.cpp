/**
 * @file
 * @author Alexander Sherikov
 */

#include <iostream>

#include <fstream>
#include <cstdio>
#include <limits>
#include <cmath> // abs, M_PI
#include <cstring> //strcmp

#include "WMG.h"
#include "smpc_solver.h"

#include "zeno_igm.h"
#include "leg2joints.h"
#include "joints_sensors_id.h"


using namespace std;

#include "tests_common.cpp"
#include "draw_SDL.cpp"
#include "init_steps_zeno.cpp"


int main(int argc, char **argv)
{
    //-----------------------------------------------------------
    // sampling
    int control_sampling_time_ms = 40;
    int preview_sampling_time_ms = 80;
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    // initialize classes
    init_0888 tdata("test_055", preview_sampling_time_ms, false);

    
    vector<double> x_coord;
    vector<double> y_coord;
    vector<double> angle_rot;
    tdata.wmg->getFootsteps(x_coord, y_coord, angle_rot);

    /*
    smpc::solver_as solver(
        tdata.wmg->N, // size of the preview window
        4000.0,  // Beta
        1.0,  // Alpha
        0.02,   // regularization
        1.0,    // Gamma
        1e-7);  // tolerance
    */

    double mpc_gain_position = 100.0;   // closeness to the reference ZMP points 
    double mpc_gain_acceleration = 0.02; // penalty for the acceleration
    double mpc_gain_velocity = 1.0;      // penalty for the velocity
    double mpc_gain_jerk = 1.0;          // penalty for the jerk

    double mpc_as_tolerance = 1e-7;
    double mpc_as_max_activate = 5;
    double mpc_as_use_downdate = true;

    smpc::solver_as solver(
                tdata.wmg->N,
                mpc_gain_position,
                mpc_gain_velocity,
                mpc_gain_acceleration,
                mpc_gain_jerk,
                mpc_as_tolerance,
                mpc_as_max_activate,
                mpc_as_use_downdate,
                false); // objective
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    tdata.zeno.getCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);
    tdata.par->init_state.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    tdata.X_tilde.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    //-----------------------------------------------------------

    std::cout << "COM pos : [" << tdata.zeno.CoM_position[0] << "," << tdata.zeno.CoM_position[1] << "," << tdata.zeno.CoM_position[2] << "]"<< std::endl;
    smpc::state_com next_CoM;
    next_CoM.set(tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);


    initSDL();
    //-----------------------------------------------------------
    // output
    test_log log(tdata.fs_out_filename.c_str());
    //-----------------------------------------------------------

    isRunning=1;
    tdata.wmg->T_ms[0] = control_sampling_time_ms;
    tdata.wmg->T_ms[1] = control_sampling_time_ms;
    Transform<double,3> foot_posture;
    tdata.zeno.getPosture(tdata.zeno.state_sensor, foot_posture.data());

    Matrix4d posture = Matrix4d::Map(foot_posture.data());
    Matrix3d rotation = posture.corner(TopLeft,3,3);
    //angle = rotation.eulerAngles(0,1,2)[2];

    Eigen::Matrix<double,3,1> euler_ref = rotation.eulerAngles(0,1,2);
    if (euler_ref[0] > 1.0)
            euler_ref[0] -= 3.14156;

    if (euler_ref[1] > 2.0)
        euler_ref[1] -= 3.14156;
    else if (euler_ref[1] < -2.0)
        euler_ref[1] += 3.14156;

    if (euler_ref[2] > 2.0)
        euler_ref[2] -= 3.14156;
    else if (euler_ref[2] < -2.0)
        euler_ref[2] += 3.14156;

    while (isRunning)
    {
        tdata.zeno.state_sensor = tdata.zeno.state_model;

        if (tdata.wmg->formPreviewWindow(*tdata.par) == WMG_HALT)
        {
            cout << "EXIT (halt = 1)" << endl;
            break;
        }
        for (unsigned int j = 0; j < tdata.wmg->N; j++)
        {
            log.addZMPrefPoint(tdata.par->zref_x[j], tdata.par->zref_y[j]);
        }

        if (tdata.wmg->isSupportSwitchNeeded())
        {
            Transform<double,3> swing_foot_posture;
            tdata.zeno.getSwingFootPosture (tdata.zeno.state_sensor, swing_foot_posture.data());
            tdata.wmg->changeNextSSPosition (swing_foot_posture.data(), true);

            
            tdata.zeno.switchSupportFoot();
        } else {
            
        }
        
       
        
        

        // position of CoM
        
        /*tdata.zeno.getWaistCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);
        smpc::state_com state_waist;
        state_waist.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);

        tdata.zeno.getCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);
        smpc::state_com state_com;
        state_com.set (tdata.zeno.CoM_position[0],tdata.zeno.CoM_position[1]);

        smpc::state_com waist_error_com;
        waist_error_com.set ((state_com.x()-state_waist.x()),
                            (state_com.y()-state_waist.y()));*/
        tdata.zeno.getCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);

        smpc::state_com state_error;
        state_error.set ((tdata.par->init_state.x() - tdata.zeno.CoM_position[0]),
                        (tdata.par->init_state.y() - tdata.zeno.CoM_position[1]));

        
        //std::cout << "state_error expected: [" <<  state_error.x() << "," << state_error.y() << "]" << std::endl;
        log.addCoMSupportChange(state_error.x(), state_error.y());

        /*if (state_error1.x() > state_error.x())
        {
            state_error.x() -= 0.0015;
        }
        else if (state_error1.x() < -0.0015)
        {
            state_error.x() += 0.0015;
        }
        else
        {
            state_error.x() = 0.0;
        }
        //tdata.par->init_state.x() += 0.8 * state_error.x();
        
        if (state_error.y() > 0.005)
        {
            state_error.y() -= 0.005;
        }
        else if (state_error.y() < -0.005)
        {
            state_error.y() += 0.005;
        }
        else
        {
            state_error.y() = 0.0;
        }*/
        //tdata.par->init_state.y() -= 0.35 * state_error.y();
        //std::cout << "init_state after: [" << tdata.par->init_state.x() << "," << tdata.par->init_state.y() << "]" << std::endl;


        //------------------------------------------------------
        solver.set_parameters (tdata.par->T, tdata.par->h, tdata.par->h[0], tdata.par->angle, tdata.par->zref_x, tdata.par->zref_y, tdata.par->lb, tdata.par->ub);
        solver.form_init_fp (tdata.par->fp_x, tdata.par->fp_y, tdata.par->init_state, tdata.par->X);
        solver.solve();
        
        /*std::cout << "state_error: [" << tdata.par->init_state.state_vector[0] << "," 
                                      << tdata.par->init_state.state_vector[1] << "," 
                                      << tdata.par->init_state.state_vector[2] << "," 
                                      << tdata.par->init_state.state_vector[3] << "," 
                                      << tdata.par->init_state.state_vector[4] << "," 
                                      << tdata.par->init_state.state_vector[5] << "]" << std::endl;*/
        //-----------------------------------------------------------
        // update state
        solver.get_next_state(tdata.par->init_state); //com
        //std::cout << "desired com update state 0: [" << tdata.par->init_state.x() << "," << tdata.par->init_state.y() << "]" << std::endl;
        solver.get_next_state(tdata.X_tilde);  //zmp
        //-----------------------------------------------------------

        double gain_pitch = 0.0;
        double gain_roll = 0.0;
        double threshold_pitch = 0.05;
        double threshold_roll = 0.05;
        int scale = 2;
        
        //-----------------------------------------------------------
        // position of CoM
        tdata.zeno.setCoM(tdata.par->init_state.x(), tdata.par->init_state.y(), tdata.par->hCoM-0.002);

        // support foot and swing foot position/orientation
        tdata.wmg->getFeetPositions (
                control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWithWaistPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igmWithWaistRollPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igm(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            //break;
        }
        int failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            //break;
        }  else {
                Transform<double,3> foot_posture;
                tdata.zeno.getPosture(tdata.zeno.state_sensor, foot_posture.data());

                Matrix4d posture = Matrix4d::Map(foot_posture.data());
                Matrix3d rotation = posture.corner(TopLeft,3,3);
                //angle = rotation.eulerAngles(0,1,2)[2];

                Eigen::Matrix<double,3,1> euler = rotation.eulerAngles(0,1,2);
                if (euler[0] > 1.0)
                        euler[0] -= 3.14156;

                if (euler[1] > 2.0)
                    euler[1] -= 3.14156;
                else if (euler[1] < -2.0)
                    euler[1] += 3.14156;

                if (euler[2] > 2.0)
                    euler[2] -= 3.14156;
                else if (euler[2] < -2.0)
                    euler[2] += 3.14156;

                log.addWaistOrientation(euler[0], euler[1], euler[2]);
                //cout << "waist orientation IGM_SUPPORT_RIGHT: " <<  euler_ref[0] - euler[0] << "," << endl;
                                       //rotation.eulerAngles(0,1,2)[1] << "," << 
                                       //rotation.eulerAngles(0,1,2)[2] << "," <<  endl;
                double error_roll = euler_ref[0] - euler[0];
                double error_pitch = euler_ref[1] - euler[1];
                double threshold_pitch_min = euler_ref[1] - threshold_pitch;
                double threshold_pitch_max = euler_ref[1] + threshold_pitch;
                int current_step_type;
                double time_left;
                cout << "waist orientation error pitch 1: " <<  error_pitch << "," << endl;

                tdata.wmg->getCurrentStepInfo(current_step_type, time_left);

                if ((error_pitch < threshold_pitch_min) or (error_pitch > threshold_pitch_max)){
                    //if (tdata.wmg->getCurrentStepType() == FS_TYPE_SS_R)
                    if ((current_step_type==FS_TYPE_SS_R) and (time_left > scale*preview_sampling_time_ms))
                    {
                        tdata.zeno.state_model.q[R_HIP_PITCH] -= gain_pitch * (error_pitch+threshold_pitch);
                        //cout << "waist orientation IGM_SUPPORT_RIGHT: " <<  error_pitch << "," << endl;

                    } else if ((current_step_type==FS_TYPE_SS_L) and (time_left > scale*preview_sampling_time_ms)){
                        tdata.zeno.state_model.q[L_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                        //cout << "waist orientation IGM_SUPPORT_LEFT: " <<  error_pitch << "," << endl;
                    } else if (current_step_type==FS_TYPE_DS) {
                        tdata.zeno.state_model.q[R_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                        tdata.zeno.state_model.q[L_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                    }
                }

                double threshold_roll_min = euler_ref[0] - threshold_roll;
                double threshold_roll_max = euler_ref[0] + threshold_roll;

                if ((error_roll < threshold_roll_min) or (error_roll > threshold_roll_max))
                {
                    if ((current_step_type==FS_TYPE_SS_R) and (time_left > scale*preview_sampling_time_ms))
                    {
                        tdata.zeno.state_model.q[R_HIP_ROLL] -= gain_roll * error_roll;

                        tdata.zeno.state_model.q[L_HIP_PITCH] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_ANKLE_PITCH] += gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_KNEE_PITCH] -= gain_roll * error_roll;
                        //cout << "roll error IGM_SUPPORT_RIGHT: " <<  error_roll << "," << endl;
                        //cout << "hip roll IGM_SUPPORT_RIGHT: " <<  tdata.zeno.state_model.q[R_HIP_ROLL] << "," << endl;
                        //cout << "hip roll left IGM_SUPPORT_Right: " <<  tdata.zeno.state_model.q[L_HIP_ROLL] << "," << endl;

                    } else if ((current_step_type==FS_TYPE_SS_L) and (time_left > scale*preview_sampling_time_ms)) {
                        tdata.zeno.state_model.q[L_HIP_ROLL] -= gain_roll * error_roll;

                        tdata.zeno.state_model.q[R_HIP_PITCH] += gain_roll * error_roll;
                        tdata.zeno.state_model.q[R_ANKLE_PITCH] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[R_KNEE_PITCH] += gain_roll * error_roll;
                        //cout << "roll error IGM_SUPPORT_LEFT: " <<  error_roll << "," << endl;
                        //cout << "hip roll right IGM_SUPPORT_LEFT: " <<  tdata.zeno.state_model.q[R_HIP_ROLL] << "," << endl;
                        //cout << "hip roll IGM_SUPPORT_LEFT: " <<  tdata.zeno.state_model.q[L_HIP_ROLL] << "," << endl;
                    } else if (current_step_type==FS_TYPE_DS) {
                        tdata.zeno.state_model.q[R_HIP_ROLL] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_HIP_ROLL] -= gain_roll * error_roll;
                    }
                }

                log.addCoMpoint (tdata.par->init_state.x(), tdata.par->init_state.y());
                log.addZMPpoint (tdata.X_tilde.x(), tdata.X_tilde.y());
                log.addFeetPositions (tdata.zeno);
        }
        //-----------------------------------------------------------
        //-----------------------------------------------------------
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {

            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }


        
        //-----------------------------------------------------------
        // position of CoM
        
        smpc::state_zmp next_ZMP;
        solver.get_state (next_CoM, 1);
        solver.get_state (next_ZMP, 1);
        //std::cout << "desired com update state 1: [" << next_CoM.x() << "," << next_CoM.y() << "]" << std::endl;
        //std::cout << "state_error end: [" << tdata.par->init_state.x()-next_CoM.x() << ",s" << tdata.par->init_state.y()-next_CoM.y() << "]" << std::endl;
        //std::cout << "========================================================" << std::endl;

        tdata.zeno.setCoM(next_CoM.x(), next_CoM.y(), tdata.par->hCoM-0.002);

        tdata.wmg->getFeetPositions (
                2*control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWithWaistPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igmWithWaistRollPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igm(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            //break;
        }
        failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            //break;
        } else {

                Transform<double,3> foot_posture;
                tdata.zeno.getPosture(tdata.zeno.state_sensor, foot_posture.data());

                Matrix4d posture = Matrix4d::Map(foot_posture.data());
                Matrix3d rotation = posture.corner(TopLeft,3,3);
                //angle = rotation.eulerAngles(0,1,2)[2];

                Eigen::Matrix<double,3,1> euler = rotation.eulerAngles(0,1,2);
                if (euler[0] > 1.0)
                        euler[0] -= 3.14156;

                if (euler[1] > 2.0)
                    euler[1] -= 3.14156;
                else if (euler[1] < -2.0)
                    euler[1] += 3.14156;

                if (euler[2] > 2.0)
                    euler[2] -= 3.14156;
                else if (euler[2] < -2.0)
                    euler[2] += 3.14156;

                log.addWaistOrientation(euler[0], euler[1], euler[2]);
                //cout << "waist orientation IGM_SUPPORT_RIGHT: " <<  euler_ref[0] - euler[0] << "," << endl;
                                       //rotation.eulerAngles(0,1,2)[1] << "," << 
                                       //rotation.eulerAngles(0,1,2)[2] << "," <<  endl;
                double error_roll = euler_ref[0] - euler[0];
                double error_pitch = euler_ref[1] - euler[1];
                double threshold_pitch_min = euler_ref[1] - threshold_pitch;
                double threshold_pitch_max = euler_ref[1] + threshold_pitch;
                int current_step_type;
                double time_left;
                cout << "waist orientation error pitch 1: " <<  error_pitch << "," << endl;

                tdata.wmg->getCurrentStepInfo(current_step_type, time_left);

                if ((error_pitch < threshold_pitch_min) or (error_pitch > threshold_pitch_max)){
                    //if (tdata.wmg->getCurrentStepType() == FS_TYPE_SS_R)
                    if ((current_step_type==FS_TYPE_SS_R) and (time_left > scale*preview_sampling_time_ms))
                    {
                        tdata.zeno.state_model.q[R_HIP_PITCH] -= gain_pitch * (error_pitch+threshold_pitch);
                        //cout << "waist orientation IGM_SUPPORT_RIGHT: " <<  error_pitch << "," << endl;

                    } else if ((current_step_type==FS_TYPE_SS_L) and (time_left > scale*preview_sampling_time_ms)){
                        tdata.zeno.state_model.q[L_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                        //cout << "waist orientation IGM_SUPPORT_LEFT: " <<  error_pitch << "," << endl;
                    } else if (current_step_type==FS_TYPE_DS) {
                        tdata.zeno.state_model.q[R_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                        tdata.zeno.state_model.q[L_HIP_PITCH] += gain_pitch * (error_pitch-threshold_pitch);
                    }
                }

                double threshold_roll_min = euler_ref[0] - threshold_roll;
                double threshold_roll_max = euler_ref[0] + threshold_roll;

                if ((error_roll < threshold_roll_min) or (error_roll > threshold_roll_max))
                {
                    if ((current_step_type==FS_TYPE_SS_R) and (time_left > scale*preview_sampling_time_ms))
                    {
                        tdata.zeno.state_model.q[R_HIP_ROLL] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_HIP_PITCH] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_ANKLE_PITCH] += gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_KNEE_PITCH] -= gain_roll * error_roll;
                        //cout << "roll error IGM_SUPPORT_RIGHT: " <<  error_roll << "," << endl;
                        //cout << "hip roll IGM_SUPPORT_RIGHT: " <<  tdata.zeno.state_model.q[R_HIP_ROLL] << "," << endl;
                        //cout << "hip roll left IGM_SUPPORT_Right: " <<  tdata.zeno.state_model.q[L_HIP_ROLL] << "," << endl;

                    } else if ((current_step_type==FS_TYPE_SS_L) and (time_left > scale*preview_sampling_time_ms)) {
                        tdata.zeno.state_model.q[L_HIP_ROLL] -= gain_roll * error_roll;

                        tdata.zeno.state_model.q[R_HIP_PITCH] += gain_roll * error_roll;
                        tdata.zeno.state_model.q[R_ANKLE_PITCH] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[R_KNEE_PITCH] += gain_roll * error_roll;
                        //cout << "roll error IGM_SUPPORT_LEFT: " <<  error_roll << "," << endl;
                        //cout << "hip roll right IGM_SUPPORT_LEFT: " <<  tdata.zeno.state_model.q[R_HIP_ROLL] << "," << endl;
                        //cout << "hip roll IGM_SUPPORT_LEFT: " <<  tdata.zeno.state_model.q[L_HIP_ROLL] << "," << endl;
                    } else if (current_step_type==FS_TYPE_DS) {
                        tdata.zeno.state_model.q[R_HIP_ROLL] -= gain_roll * error_roll;
                        tdata.zeno.state_model.q[L_HIP_ROLL] -= gain_roll * error_roll;
                    }
                }
                double left_shoulder_pitch = 0.0;
                double  right_shoulder_pitch = 0.0;

                /*tdata.get_arm_swing_motion(current_step_type, left_shoulder_pitch, right_shoulder_pitch);
                tdata.zeno.state_model.q[L_SHOULDER_PITCH] = left_shoulder_pitch;
                tdata.zeno.state_model.q[R_SHOULDER_PITCH] = right_shoulder_pitch;*/

                log.addCoMpoint (next_CoM.x(), next_CoM.y());
                log.addFeetPositions (tdata.zeno);
                log.addZMPpoint (next_ZMP.x(), next_ZMP.y());
        }

        //-----------------------------------------------------------
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {

            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }
        usleep (100000);
    }

    //tdata.zeno.state_model.q = tdata.zeno.state_model.q_init;

    // keep the visualization active (until ESC is pressed)
    while (isRunning)
    {
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q_init, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q_init, tdata.zeno.left_foot_posture);
        }
    }

    //-----------------------------------------------------------
    // output
    log.flushLeftFoot ();
    log.flushRightFoot ();
    log.flushZMP ();
    log.flushZMPref ();
    log.flushCoM ();
    log.flushCoMSupportChange();
    log.flushWaistOrientation();
    //-----------------------------------------------------------

    return 0;
}
/*
void oru_walk::feedbackError (smpc::state_com &init_state)
{
    if (wp.controller_type_ == WAIST_TRACKER)
        zeno.getWaistCoM (zeno.state_sensor, zeno.CoM_position);
    else if(wp.controller_type_ == COM_TRACKER)
        zeno.getCoM (zeno.state_sensor, zeno.CoM_position);
    std::cout << "init_state: [" <<  init_state.x() << "," << init_state.y() << "]" << std::endl;
    std::cout << "com_state: [" <<  zeno.CoM_position[0] << "," << zeno.CoM_position[1] << "]" << std::endl;
    smpc::state_com state_error;
    state_error.set (
            init_state.x() - zeno.CoM_position[0],
            init_state.y() - zeno.CoM_position[1]);

    
    if (state_error.x() > 0.015)
    {
        state_error.x() -= 0.015;
    }
    else if (state_error.x() < -0.015)
    {
        state_error.x() += 0.015;
    }
    else
    {
        state_error.x() = 0.0;
    }
    init_state.x() -= 0.5 * state_error.x();
    
    if (state_error.y() > 0.005)
    {
        state_error.y() -= 0.005;
    }
    else if (state_error.y() < -0.005)
    {
        state_error.y() += 0.005;
    }
    else
    {
        state_error.y() = 0.0;
    }
    init_state.y() -= wp.feedback_gain * state_error.y();
    
    log.addStateError(state_error.x(), state_error.y());
    //std::cout << "state_error: [" <<  state_error.x() << "," << state_error.y() << "]" << std::endl;
}

*/