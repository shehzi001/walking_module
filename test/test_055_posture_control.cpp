/**
 * @file
 * @author Alexander Sherikov
 */

#include <iostream>

#include <fstream>
#include <cstdio>
#include <limits>
#include <cmath> // abs, M_PI
#include <cstring> //strcmp

#include "WMG.h"
#include "smpc_solver.h"

#include "zeno_igm.h"
#include "leg2joints.h"
#include "joints_sensors_id.h"


using namespace std;

#include "tests_common.cpp"
#include "draw_SDL.cpp"
#include "init_steps_zeno.cpp"



int main(int argc, char **argv)
{
    //-----------------------------------------------------------
    // sampling
    int control_sampling_time_ms = 40;
    int preview_sampling_time_ms = 80;
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    // initialize classes
    init_0888 tdata("test_055_posture_control", preview_sampling_time_ms, false);

    
    vector<double> x_coord;
    vector<double> y_coord;
    vector<double> angle_rot;
    tdata.wmg->getFootsteps(x_coord, y_coord, angle_rot);

    /*
    smpc::solver_as solver(
        tdata.wmg->N, // size of the preview window
        4000.0,  // Beta
        1.0,  // Alpha
        0.02,   // regularization
        1.0,    // Gamma
        1e-7);  // tolerance
    */

    double mpc_gain_position = 200.0;   // closeness to the reference ZMP points 
    double mpc_gain_acceleration = 0.02; // penalty for the acceleration
    double mpc_gain_velocity = 1.0;      // penalty for the velocity
    double mpc_gain_jerk = 1.0;          // penalty for the jerk

    double mpc_as_tolerance = 1e-7;
    double mpc_as_max_activate = 5;
    double mpc_as_use_downdate = true;

    smpc::solver_as solver(
                tdata.wmg->N,
                mpc_gain_position,
                mpc_gain_velocity,
                mpc_gain_acceleration,
                mpc_gain_jerk,
                mpc_as_tolerance,
                mpc_as_max_activate,
                mpc_as_use_downdate,
                false); // objective
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    tdata.zeno.getCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);
    tdata.par->init_state.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    tdata.X_tilde.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    //-----------------------------------------------------------

    std::cout << "COM pos : [" << tdata.zeno.CoM_position[0] << "," << tdata.zeno.CoM_position[1] << "," << tdata.zeno.CoM_position[2] << "]"<< std::endl;
    smpc::state_com next_CoM;
    next_CoM.set(tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);


    initSDL();
    //-----------------------------------------------------------
    // output
    test_log log(tdata.fs_out_filename.c_str());
    //-----------------------------------------------------------

    isRunning=1;
    tdata.wmg->T_ms[0] = control_sampling_time_ms;
    tdata.wmg->T_ms[1] = control_sampling_time_ms;
    Transform<double,3> foot_posture;
    tdata.zeno.getWaistPosture(tdata.zeno.state_sensor, foot_posture.data());

    Matrix4d posture = Matrix4d::Map(foot_posture.data());
    Matrix3d rotation = posture.corner(TopLeft,3,3);
    //angle = rotation.eulerAngles(0,1,2)[2];

    Eigen::Matrix<double,3,1> euler_ref = rotation.eulerAngles(0,1,2);
    if (euler_ref[0] > 1.0)
            euler_ref[0] -= 3.14156;

    if (euler_ref[1] > 2.0)
        euler_ref[1] -= 3.14156;
    else if (euler_ref[1] < -2.0)
        euler_ref[1] += 3.14156;

    if (euler_ref[2] > 2.0)
        euler_ref[2] -= 3.14156;
    else if (euler_ref[2] < -2.0)
        euler_ref[2] += 3.14156;

    while (isRunning)
    {
        tdata.zeno.state_sensor = tdata.zeno.state_model;

        if (tdata.wmg->formPreviewWindow(*tdata.par) == WMG_HALT)
        {
            cout << "EXIT (halt = 1)" << endl;
            break;
        }
        for (unsigned int j = 0; j < tdata.wmg->N; j++)
        {
            log.addZMPrefPoint(tdata.par->zref_x[j], tdata.par->zref_y[j]);
        }

        if (tdata.wmg->isSupportSwitchNeeded())
        {
            Transform<double,3> swing_foot_posture;
            tdata.zeno.getSwingFootPosture (tdata.zeno.state_sensor, swing_foot_posture.data());
            tdata.wmg->changeNextSSPosition (swing_foot_posture.data(), true);

            
            tdata.zeno.switchSupportFoot();
        } 

        tdata.zeno.getCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);

        //------ computer posture_control command
        Transform<double,3> foot_posture;
        tdata.zeno.getWaistPosture(tdata.zeno.state_sensor, foot_posture.data());

        Matrix4d posture = Matrix4d::Map(foot_posture.data());
        Matrix3d rotation = posture.corner(TopLeft,3,3);

        //posture_control(rotation, tdata.par->init_state, tdata.par->T);
        Eigen::Matrix<double,3,1> euler = rotation.eulerAngles(0,1,2);
        if (euler[0] > 1.0)
                euler[0] -= 3.14156;

        if (euler[1] > 2.0)
            euler[1] -= 3.14156;
        else if (euler[1] < -2.0)
            euler[1] += 3.14156;

        if (euler[2] > 2.0)
            euler[2] -= 3.14156;
        else if (euler[2] < -2.0)
            euler[2] += 3.14156;

        //cout << "waist orientation IGM_SUPPORT_RIGHT: " <<  euler_ref[0] - euler[0] << "," << endl;
                               //rotation.eulerAngles(0,1,2)[1] << "," << 
                               //rotation.eulerAngles(0,1,2)[2] << "," <<  endl;
        
        double error_roll = euler[0];
        double error_pitch = euler[1];

        cout << "waist orientation(roll, pitch): " << error_roll <<  "," << error_pitch << "," << endl;

                    
        double threshold_roll_min = -0.008;
        double threshold_roll_max = 0.008;

        double P = 0.15;

        double Yam = P * 0.08 * error_roll;
        double Xam = 1.5 * 0.08 * error_pitch;

        /*if (abs(error_pitch) > 0.0008 )
        {   
           
        } else {
                Xam = 0.0;
        }*/

        if (abs(error_roll) > 0.17 )
        {   
            /*if (Yam < threshold_roll_min)
            {
                Yam = threshold_roll_min;
            } else if (Yam > threshold_roll_max){
                Yam = threshold_roll_max;
            }*/
        } else {
                Yam = 0.0;
        }

        cout << "Momentum controller(Xam, Yam): " << Xam <<  "," << Yam << "," << endl;

        tdata.par->init_state.y() += Yam;
        //tdata.par->init_state.x() -= Xam;
        

        //------------------------------------------------------
        solver.set_parameters (tdata.par->T, tdata.par->h, tdata.par->h[0], tdata.par->angle, tdata.par->zref_x, tdata.par->zref_y, tdata.par->lb, tdata.par->ub);
        solver.form_init_fp (tdata.par->fp_x, tdata.par->fp_y, tdata.par->init_state, tdata.par->X);
        solver.solve();
        
        /*std::cout << "state_error: [" << tdata.par->init_state.state_vector[0] << "," 
                                      << tdata.par->init_state.state_vector[1] << "," 
                                      << tdata.par->init_state.state_vector[2] << "," 
                                      << tdata.par->init_state.state_vector[3] << "," 
                                      << tdata.par->init_state.state_vector[4] << "," 
                                      << tdata.par->init_state.state_vector[5] << "]" << std::endl;*/
        //-----------------------------------------------------------
        // update state
        solver.get_next_state(tdata.par->init_state); //com
        //std::cout << "desired com update state 0: [" << tdata.par->init_state.x() << "," << tdata.par->init_state.y() << "]" << std::endl;
        solver.get_next_state(tdata.X_tilde);  //zmp
        //-----------------------------------------------------------

        // position of CoM
        tdata.zeno.setCoM(tdata.par->init_state.x(), tdata.par->init_state.y(), tdata.par->hCoM-0.002);

        // support foot and swing foot position/orientation
        tdata.wmg->getFeetPositions (
                control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWithWaistPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igmWithWaistRollPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igm(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            break;
        }
        int failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            break;
        }  else {
               

                log.addCoMpoint (tdata.par->init_state.x(), tdata.par->init_state.y());
                log.addZMPpoint (tdata.X_tilde.x(), tdata.X_tilde.y());
                log.addFeetPositions (tdata.zeno);
        }
        //-----------------------------------------------------------
        //-----------------------------------------------------------
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {

            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }

        
        //-----------------------------------------------------------
        // position of CoM
        
        smpc::state_zmp next_ZMP;
        solver.get_state (next_CoM, 1);
        solver.get_state (next_ZMP, 1);
        next_CoM.y() += Yam;
        next_CoM.x() += Xam;

        tdata.zeno.setCoM(next_CoM.x(), next_CoM.y(), tdata.par->hCoM-0.002);

        tdata.wmg->getFeetPositions (
                2*control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWithWaistPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igmWithWaistRollPitchConstraint(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        //if (tdata.zeno.igm(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            break;
        }
        failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            break;
        } else {
                int current_step_type;
                double time_left;

                tdata.wmg->getCurrentStepInfo(current_step_type, time_left);

                double left_shoulder_pitch = 0.0;
                double  right_shoulder_pitch = 0.0;

                tdata.get_arm_swing_motion(current_step_type, left_shoulder_pitch, right_shoulder_pitch);
                tdata.zeno.state_model.q[L_SHOULDER_PITCH] = left_shoulder_pitch;
                tdata.zeno.state_model.q[R_SHOULDER_PITCH] = right_shoulder_pitch;


                log.addCoMpoint (next_CoM.x(), next_CoM.y());
                log.addFeetPositions (tdata.zeno);
                log.addZMPpoint (next_ZMP.x(), next_ZMP.y());
        }

        //-----------------------------------------------------------
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {

            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }
        usleep (100000);
    }

    //tdata.zeno.state_model.q = tdata.zeno.state_model.q_init;

    // keep the visualization active (until ESC is pressed)
    while (isRunning)
    {
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q_init, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q_init, tdata.zeno.left_foot_posture);
        }
    }

    //-----------------------------------------------------------
    // output
    log.flushLeftFoot ();
    log.flushRightFoot ();
    log.flushZMP ();
    log.flushZMPref ();
    log.flushCoM ();
    log.flushCoMSupportChange();
    log.flushWaistOrientation();
    //-----------------------------------------------------------

    return 0;
}

