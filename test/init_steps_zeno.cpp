#include <cmath>


void initZenoModel (zeno_igm& zeno, double* ref_angles)
{
    // joint angles -0.335088, 0.785761, -0.416376

    zeno.state_sensor.q[L_HIP_ROLL]       =       ref_angles[L_HIP_ROLL]    =  0.0;
    zeno.state_sensor.q[L_HIP_YAW]        =       ref_angles[L_HIP_YAW]     =  0.0;
    zeno.state_sensor.q[L_HIP_PITCH]      =       ref_angles[L_HIP_PITCH]   =  -0.35; //-0.436332;
    zeno.state_sensor.q[L_KNEE_PITCH]     =       ref_angles[L_KNEE_PITCH]  =  0.75;  //0.436332;
    zeno.state_sensor.q[L_ANKLE_PITCH]    =       ref_angles[L_ANKLE_PITCH] =  -0.416; //-0.436332/4;
    zeno.state_sensor.q[L_ANKLE_ROLL]     =       ref_angles[L_ANKLE_ROLL]  =  0.0;
    
    zeno.state_sensor.q[R_HIP_ROLL]       =       ref_angles[R_HIP_ROLL]    =  0.0;
    zeno.state_sensor.q[R_HIP_YAW]        =       ref_angles[R_HIP_YAW]     =  0.0;
    zeno.state_sensor.q[R_HIP_PITCH]      =       ref_angles[R_HIP_PITCH]   =  -0.35; //-0.436332;
    zeno.state_sensor.q[R_KNEE_PITCH]     =       ref_angles[R_KNEE_PITCH]  =  0.75; //0.436332;
    zeno.state_sensor.q[R_ANKLE_PITCH]    =       ref_angles[R_ANKLE_PITCH] =  -0.416;//-0.436332/4;
    zeno.state_sensor.q[R_ANKLE_ROLL]     =       ref_angles[R_ANKLE_ROLL]  =  0.0;

    zeno.state_sensor.q[L_SHOULDER_PITCH] =      1.0;//0.78539;
    zeno.state_sensor.q[L_SHOULDER_ROLL]  =      0.25;//0.78539;
    zeno.state_sensor.q[L_ELBOW_ROLL]     =       0.0;
    zeno.state_sensor.q[L_ELBOW_YAW]      =       -0.1;//-0.78539;
    zeno.state_sensor.q[L_WRIST_YAW]      =       0.0;

    zeno.state_sensor.q[R_SHOULDER_PITCH] =      1.0;//0.78539;
    zeno.state_sensor.q[R_SHOULDER_ROLL]  =      -0.25;//-0.78539;
    zeno.state_sensor.q[R_ELBOW_ROLL]     =       0.0;
    zeno.state_sensor.q[R_ELBOW_YAW]      =      0.1;//0.78539;
    zeno.state_sensor.q[R_WRIST_YAW]      =       0.0;

    zeno.state_sensor.q[HEAD_YAW]         =       0.0;
    zeno.state_sensor.q[HEAD_PITCH]       =       0.0;
    zeno.state_sensor.q[HEAD_ROLL]        =       0.0;

    zeno.state_sensor.q[TORSO_YAW]        =       0.0;

}



class test_init_base
{
    public:
        test_init_base(const string& test_name, const bool plot_ds_)
        {
            name = test_name;
            plot_ds = plot_ds_;

            if (!name.empty())
            {
                cout << "################################" << endl;
                cout << name << endl;
                cout << "################################" << endl;
                fs_out_filename = name + "_fs.m";
            }
        }
        ~test_init_base()
        {
            if (!name.empty())
            {
                cout << "################################" << endl;
            }
        }



        smpc::state_zmp X_tilde;
        WMG* wmg;
        smpc_parameters* par;

        zeno_igm zeno;
        double ref_angles[LOWER_JOINTS_NUM];

        string name;
        string fs_out_filename;
        bool plot_ds;
};



/**
 * @brief Walk straight
 */
class init_08 : public test_init_base
{
    public:
        init_08 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getCoM(zeno.state_sensor, zeno.CoM_position);

            double bezier_weight_1 = 1.5;
            double bezier_weight_2 = 3.0;
            double bezier_inclination_1 = 0.015;
            double bezier_inclination_2 = 0.01;

            wmg = new WMG (40, preview_sampling_time_ms, 0.02,
                            bezier_weight_1 = 1.5,
                            bezier_weight_2 = 3.0,
                            bezier_inclination_1 = 0.015,
                            bezier_inclination_2 = 0.01
                );


            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]);
            int ss_time_ms = 400;
            int ds_time_ms = 40;
            int ds_number = 3;

            // each step is defined relatively to the previous step
            double step_x = 0.05;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position

    
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_SS_R);

            // Initial double support
            wmg->setFootstepParametersMS (3*ss_time_ms, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_DS);


            // all subsequent steps have normal feet size
            wmg->setFootstepParametersMS (ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0);
            wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
            wmg->addFootstep(step_x,  -step_y, 0.0);


            for (int i = 0; i < 3; i++)
            {
                wmg->addFootstep(step_x,  step_y, 0.0);
                wmg->addFootstep(step_x, -step_y, 0.0);
            }

            // here we give many reference points, since otherwise we 
            // would not have enough steps in preview window to reach 
            // the last footsteps
            wmg->setFootstepParametersMS (5*ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_SS_L);
   /*
    wmg->setFootstepParametersMS(0, 0, 0);
    wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

    // Initial double support
    wmg->setFootstepParametersMS(3*ss_time_ms, 0, 0);
    wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


    // all subsequent steps have normal feet size
    wmg->setFootstepParametersMS(ss_time_ms, 0, 0);
    wmg->addFootstep(0.0   , -step_y/2, 0.0);
    wmg->setFootstepParametersMS(ss_time_ms, ds_time_ms, ds_number);
    wmg->addFootstep(step_x,  step_y,   0.0);

    for (int i = 0; i < 4; i++)
    {
        wmg->addFootstep(step_x, -step_y, 0.0);
        wmg->addFootstep(step_x,  step_y, 0.0);
    }

    // here we give many reference points, since otherwise we 
    // would not have enough steps in preview window to reach 
    // the last footsteps
    wmg->setFootstepParametersMS(5*ss_time_ms, 0, 0);
    wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
    wmg->setFootstepParametersMS(0, 0, 0);
    wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
    */

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
};

/**
 * @brief Walk Test
 */
class init_088 : public test_init_base
{
    public:
        init_088 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            // sampling
            //int control_sampling_time_ms = 80;
            //preview_sampling_time_ms = 80;
            //----------------------------------
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getCoM(zeno.state_sensor, zeno.CoM_position);
            int N = 20;

            double bezier_weight_1 = 1.5;
            double bezier_weight_2 = 3.0;
            double bezier_inclination_1 = 0.015;
            double bezier_inclination_2 = 0.01;

            wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                            bezier_weight_1,
                            bezier_weight_2,
                            bezier_inclination_1,
                            bezier_inclination_2
                );

            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]-0.002);
            int ss_time_ms = 800;
            int ds_time_ms = 80;
            int ds_number = 3;

            // each step is defined relatively to the previous step
            double step_x = 0.05;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position

            /** 
             * @brief Set default parameters of footsteps.
             *
             * @param[in] def_time_ms_ default time spent in a support (SS or DS depending on the 
             *              type of added footstep)
             * @param[in] ds_time_ms_ default time spent in an automatically generated DS.
             * @param[in] ds_number number of DS to be generated automatically.
             */
            wmg->setFootstepParametersMS (0, 0, 0);

            /**
            * @brief Adds a footstep to FS.
            *
            * @param[in] x_relative x_relative X position [meter] relative to the previous footstep.
            * @param[in] y_relative y_relative Y position [meter] relative to the previous footstep.
            * @param[in] angle_relative angle_relative Angle [rad.] relative to the previous footstep.
            * @param[in] type (optional) type of the footstep.
            *
            * @note Coordinates and angle are treated as absolute for the first step in the preview window.
            */
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_SS_R);

            // Initial double support
            wmg->setFootstepParametersMS (5*400, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_DS);


            // all subsequent steps have normal feet size
            wmg->setFootstepParametersMS (ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0);

           wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
           wmg->addFootstep(step_x,  -step_y, 0.0);

           /*wmg->setFootstepParametersMS (5*ds_time_ms, 0, 0);
           wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_DS);
           wmg->setFootstepParametersMS (0, 0, 0);
           wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_SS_L);
           //wmg->addFootstep(step_x,  step_y, 0.0);*/
           
            for (int i = 0; i < 3; i++)
            {
                wmg->addFootstep(step_x,  step_y, 0.0);
                wmg->addFootstep(step_x, -step_y, 0.0);
            }

            // here we give many reference points, since otherwise we 
            // would not have enough steps in preview window to reach 
            // the last footsteps
            wmg->setFootstepParametersMS (6*400, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0   , step_y/2, 0.0, FS_TYPE_SS_L);
            

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
};


/**
 * @brief Straight Walk Test with com based igm
 */
class init_0888 : public test_init_base
{
    public:
        init_0888 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            // sampling
            //int control_sampling_time_ms = 80;
            //preview_sampling_time_ms = 80;
            //----------------------------------
            initZenoModel (zeno, ref_angles);
            bool left_support_foot = false;
            if (left_support_foot) {
                // support foot position and orientation
                zeno.init (
                        IGM_SUPPORT_LEFT,
                        0.0, 0.04212, 0.0,
                        0.0, 0.0, 0.0);
                zeno.getCoM(zeno.state_sensor, zeno.CoM_position);
                int N = 20;
                init_arm_swing_motion(N, 1, -1);

                double bezier_weight_1 = 1.5;
                double bezier_weight_2 = 3.0;
                double bezier_inclination_1 = 0.015;
                double bezier_inclination_2 = 0.01;

                wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                                bezier_weight_1,
                                bezier_weight_2,
                                bezier_inclination_1,
                                bezier_inclination_2
                    );

                par = new smpc_parameters (wmg->N, zeno.CoM_position[2]-0.002);
                int ss_time_ms = 800;
                int ds_time_ms = 80;
                int ds_number = 4;
                int step_pairs_number = 5;

                // each step is defined relatively to the previous step
                double step_x = 0.04;      // relative X position
                double step_y = wmg->def_constraints.support_distance_y;       // relative Y position


                wmg->setFootstepParametersMS (0, 0, 0);
                wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

                // Initial double support
                wmg->setFootstepParametersMS (5.5*400, 0, 0);
                wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


                // all subsequent steps have normal feet size
                wmg->setFootstepParametersMS (ss_time_ms, 0, 0); // right support foot
                wmg->addFootstep(0.0   , -step_y/2, 0.0);

                wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
                wmg->addFootstep(step_x,  step_y, 0.0);

                //wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
                //wmg->addFootstep(-step_x*0.2,  step_y, 0.0);

                for (int i = 0; i < step_pairs_number; i++)
                {
                    wmg->addFootstep(step_x, -step_y, 0.0);
                    wmg->addFootstep(step_x,  step_y, 0.0);
                }
                wmg->setFootstepParametersMS (5.5*400, 0, 0);
                wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
                wmg->setFootstepParametersMS (0, 0, 0);
                wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);

            } else {
                // support foot position and orientation
                zeno.init (
                        IGM_SUPPORT_RIGHT,
                        0.0, -0.04212, 0.0,
                        0.0, 0.0, 0.0);
                zeno.getCoM(zeno.state_sensor, zeno.CoM_position);
                int N = 20;
                init_arm_swing_motion(N, 1, -1);

                double bezier_weight_1 = 1.5;
                double bezier_weight_2 = 3.0;
                double bezier_inclination_1 = 0.015;
                double bezier_inclination_2 = 0.01;

                wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                                bezier_weight_1,
                                bezier_weight_2,
                                bezier_inclination_1,
                                bezier_inclination_2
                    );

                par = new smpc_parameters (wmg->N, zeno.CoM_position[2]-0.002);
                int ss_time_ms = 800;
                int ds_time_ms = 80;
                int ds_number = 4;
                int step_pairs_number = 5;

                // each step is defined relatively to the previous step
                double step_x = 0.04;      // relative X position
                double step_y = wmg->def_constraints.support_distance_y;       // relative Y position


                wmg->setFootstepParametersMS (0, 0, 0);
                wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_SS_R);

                // Initial double support
                wmg->setFootstepParametersMS (5.5*400, 0, 0);
                wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_DS);


                // all subsequent steps have normal feet size
                wmg->setFootstepParametersMS (ss_time_ms, 0, 0); // right support foot
                wmg->addFootstep(0.0   , step_y/2, 0.0);

                wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
                wmg->addFootstep(step_x,  -step_y, 0.0);

                //wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
                //wmg->addFootstep(-step_x*0.2,  step_y, 0.0);

                for (int i = 0; i < step_pairs_number; i++)
                {
                    wmg->addFootstep(step_x, step_y, 0.0);

                    //if (i==(step_pairs_number-1))
                    //    wmg->addFootstep(step_x, -step_y-0.006, 0.0);
                    //else
                        wmg->addFootstep(step_x, -step_y, 0.0);
                }

                wmg->setFootstepParametersMS (5.5*400, 0, 0);
                wmg->addFootstep(0.0   , (step_y/2)+0.006, 0.0, FS_TYPE_DS);
                wmg->setFootstepParametersMS (0, 0, 0);
                wmg->addFootstep(0.0   , (step_y/2)+0.006, 0.0, FS_TYPE_SS_L);
            }
            

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }

        void init_arm_swing_motion(int N, int left_arm_direction, int right_arm_direction)
        {
            arm_swing_motion_buffer.resize(N);
            double arm_swing_min = 1.17;
            double arm_swing_max = 1.65;
            double step_size = (arm_swing_max-arm_swing_min)/N;
            double step_increment = 0.0;

            for (int i = 0; i < N; i++)
            {
                arm_swing_motion_buffer[i] = arm_swing_min+step_increment;
                step_increment += step_size;
            }
            left_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
            right_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
            left_arm_swing_direction_ = left_arm_direction;
            right_arm_swing_direction_ = right_arm_direction;
        }

        void get_arm_swing_motion(int support_foot, double &left_arm, double &right_arm)
        {
            if(support_foot == 1 or support_foot == 2) {

                if (left_arm_swing_index_ < 0) {
                    left_arm_swing_direction_ = 1;
                    left_arm_swing_index_ = 0;
                } else if (left_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
                    left_arm_swing_direction_ = -1;
                    left_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
                }

                if (right_arm_swing_index_ < 0) {
                    right_arm_swing_direction_ = 1;
                    right_arm_swing_index_ = 0;
                } else if (right_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
                    right_arm_swing_direction_ = -1;
                    right_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
                }
            }

            left_arm = arm_swing_motion_buffer[left_arm_swing_index_];
            right_arm = arm_swing_motion_buffer[right_arm_swing_index_];

            if (support_foot == 1 or support_foot == 2) {
                left_arm_swing_index_ += left_arm_swing_direction_;
                right_arm_swing_index_ += right_arm_swing_direction_;
            }
        }

        std::vector<double> arm_swing_motion_buffer;
        int left_arm_swing_index_;
        int right_arm_swing_index_;
        int left_arm_swing_direction_;
        int right_arm_swing_direction_;
};

/**
 * @brief Straight Walk Test with waist based igm
 */

class init_waist : public test_init_base
{
    public:
        init_waist (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            // sampling
            //int control_sampling_time_ms = 80;
            //preview_sampling_time_ms = 80;
            //----------------------------------
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getWaistCoM(zeno.state_sensor, zeno.CoM_position);
            int N = 20;

            double bezier_weight_1 = 1.5;
            double bezier_weight_2 = 3.0;
            double bezier_inclination_1 = 0.0;
            double bezier_inclination_2 = 0.0;

            wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                            bezier_weight_1,
                            bezier_weight_2,
                            bezier_inclination_1,
                            bezier_inclination_2
                );

            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]);
            int ss_time_ms = 800;
            int ds_time_ms = 80;
            int ds_number = 5;
            int step_pairs_number = 6;

            // each step is defined relatively to the previous step
            double step_x = 0.04;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position


            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

            // Initial double support
            wmg->setFootstepParametersMS (6*400, 0, 0);
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


            // all subsequent steps have normal feet size
            wmg->setFootstepParametersMS (ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0);
            wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
            wmg->addFootstep(step_x,  step_y, 0.0);

            for (int i = 0; i < step_pairs_number; i++)
            {
                wmg->addFootstep(step_x, -step_y, 0.0);
                wmg->addFootstep(step_x,  step_y, 0.0);
            }

            wmg->setFootstepParametersMS (5.5*400, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
            

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
};



/**
 * @brief Diagonal walk
 */
class init_09 : public test_init_base
{
    public:
        init_09 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getCoM(zeno.state_sensor, zeno.CoM_position);
            int N = 20;
            init_arm_swing_motion(N, 1, -1);

            double bezier_weight_1 = 1.5;
            double bezier_weight_2 = 3.0;
            double bezier_inclination_1 = 0.015;
            double bezier_inclination_2 = 0.01;

            wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                            bezier_weight_1,
                            bezier_weight_2,
                            bezier_inclination_1,
                            bezier_inclination_2
                );

            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]-0.002);
            int ss_time_ms = 800;
            int ds_time_ms = 80;
            int ds_number = 3;

            // each step is defined relatively to the previous step
            double step_x = 0.02;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position

            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

            // Initial double support
            wmg->setFootstepParametersMS (5.5*400, 0, 0);
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);

            // each step is defined relatively to the previous step
            double shift = -0.01;

            // all subsequent steps have normal feet size
            wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
            wmg->addFootstep(0.0   , -step_y/2, 0.0);
            wmg->addFootstep(step_x,  step_y + shift, 0.0);
            wmg->addFootstep(step_x, -step_y + shift, 0.0);
            wmg->addFootstep(step_x,  step_y + shift, 0.0);
            wmg->addFootstep(step_x, -step_y + shift, 0.0);
            wmg->addFootstep(step_x,  step_y + shift, 0.0);
            wmg->addFootstep(step_x, -step_y + shift, 0.0);
            wmg->addFootstep(step_x,  step_y + shift, 0.0);
            wmg->addFootstep(step_x, -step_y + shift, 0.0);
            wmg->addFootstep(step_x,  step_y + shift, 0.0);

            wmg->setFootstepParametersMS (5.5*400, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
            

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
        void init_arm_swing_motion(int N, int left_arm_direction, int right_arm_direction)
        {
            arm_swing_motion_buffer.resize(N);
            double arm_swing_min = 1.17;
            double arm_swing_max = 1.65;
            double step_size = (arm_swing_max-arm_swing_min)/N;
            double step_increment = 0.0;

            for (int i = 0; i < N; i++)
            {
                arm_swing_motion_buffer[i] = arm_swing_min+step_increment;
                step_increment += step_size;
            }
            left_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
            right_arm_swing_index_ = (arm_swing_motion_buffer.size()/2);
            left_arm_swing_direction_ = left_arm_direction;
            right_arm_swing_direction_ = right_arm_direction;
        }

        void get_arm_swing_motion(int support_foot, double &left_arm, double &right_arm)
        {
            if(support_foot == 1 or support_foot == 2) {

                if (left_arm_swing_index_ < 0) {
                    left_arm_swing_direction_ = 1;
                    left_arm_swing_index_ = 0;
                } else if (left_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
                    left_arm_swing_direction_ = -1;
                    left_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
                }

                if (right_arm_swing_index_ < 0) {
                    right_arm_swing_direction_ = 1;
                    right_arm_swing_index_ = 0;
                } else if (right_arm_swing_index_ >= arm_swing_motion_buffer.size()) {
                    right_arm_swing_direction_ = -1;
                    right_arm_swing_index_ = arm_swing_motion_buffer.size()-1;
                }
            }

            left_arm = arm_swing_motion_buffer[left_arm_swing_index_];
            right_arm = arm_swing_motion_buffer[right_arm_swing_index_];

            if (support_foot == 1 or support_foot == 2) {
                left_arm_swing_index_ += left_arm_swing_direction_;
                right_arm_swing_index_ += right_arm_swing_direction_;
            }
        }

        std::vector<double> arm_swing_motion_buffer;
        int left_arm_swing_index_;
        int right_arm_swing_index_;
        int left_arm_swing_direction_;
        int right_arm_swing_direction_;
};



/**
 * @brief Circular walk. // Doesn't work on the robot.
 */
class init_10 : public test_init_base
{
    public:
        init_10 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getCoM(zeno.state_sensor, zeno.CoM_position);
            int N = 20;

            double bezier_weight_1 = 1.5;
            double bezier_weight_2 = 3.0;
            double bezier_inclination_1 = 0.015;
            double bezier_inclination_2 = 0.01;

            wmg = new WMG (N, preview_sampling_time_ms, 0.02,
                            bezier_weight_1,
                            bezier_weight_2,
                            bezier_inclination_1,
                            bezier_inclination_2
                );

            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]-0.002);
            int ss_time_ms = 800;
            int ds_time_ms = 80;
            int ds_number = 3;
            int step_pairs_number = 45;

            // each step is defined relatively to the previous step
            double step_x = 0.04;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position

            double R_ext = 0.55;
            double R_int = R_ext - step_y;

            // relative angle
            double a = asin (step_x / R_ext);
            double step_x_int = step_x * R_int / R_ext;

            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

            // Initial double support
            wmg->setFootstepParametersMS (5.5*400, 0, 0);
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);

            wmg->setFootstepParametersMS (ss_time_ms, ds_time_ms, ds_number);
            wmg->addFootstep(0.0   ,     -step_y/2, 0.0);
            wmg->addFootstep(step_x_int,  step_y, a);

            for (int i = 0; i < step_pairs_number; i++)
            {
                wmg->addFootstep(step_x, -step_y, a);
                wmg->addFootstep(step_x_int,  step_y, a);
            }

            wmg->setFootstepParametersMS (5.5*400, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS (0, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);
            

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
};



class init_11 : public test_init_base
{
    public:
        init_11 (
                const string & test_name, 
                const int preview_sampling_time_ms,
                const bool plot_ds_ = true) :
            test_init_base (test_name, plot_ds_)
        {
            initZenoModel (zeno, ref_angles);
            // support foot position and orientation
            zeno.init (
                    IGM_SUPPORT_LEFT,
                    0.0, 0.044810, 0.0,
                    0.0, 0.0, 0.0);
            zeno.getCoM(zeno.state_sensor, zeno.CoM_position);

            wmg = new WMG (40, preview_sampling_time_ms, 0.02);
            par = new smpc_parameters (wmg->N, zeno.CoM_position[2]);
            int ss_time_ms = 400;
            int ds_time_ms = 40;
            int ds_number = 3;

            // each step is defined relatively to the previous step
            double step_x = 0.04;      // relative X position
            double step_y = wmg->def_constraints.support_distance_y;       // relative Y position


            wmg->setFootstepParametersMS(0, 0, 0);
            wmg->addFootstep(0.0, step_y/2, 0.0, FS_TYPE_SS_L);

            // Initial double support
            wmg->setFootstepParametersMS(3*ss_time_ms, 0, 0);
            wmg->addFootstep(0.0, -step_y/2, 0.0, FS_TYPE_DS);


            // all subsequent steps have normal feet size
            wmg->setFootstepParametersMS(ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0);
            wmg->setFootstepParametersMS(ss_time_ms, ds_time_ms, ds_number);
            wmg->addFootstep(step_x,  step_y,   0.0);

            for (int i = 0; i < 2; i++)
            {
                wmg->addFootstep(step_x, -step_y, 0.0);
                wmg->addFootstep(step_x,  step_y, 0.0);
            }

            // here we give many reference points, since otherwise we 
            // would not have enough steps in preview window to reach 
            // the last footsteps
            wmg->setFootstepParametersMS(5*ss_time_ms, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_DS);
            wmg->setFootstepParametersMS(0, 0, 0);
            wmg->addFootstep(0.0   , -step_y/2, 0.0, FS_TYPE_SS_R);

            if (!name.empty())
            {
                wmg->FS2file(fs_out_filename, plot_ds);
            }
        }
};
