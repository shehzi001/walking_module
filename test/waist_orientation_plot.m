function [] = waist_orientation_plot(names, waist_orientation)

len = size(waist_orientation);

for i=1:len(2)
    vec = waist_orientation(:, i);
    indexmin = find(min(vec) == vec);
    indexmax = find(max(vec) == vec);
    strmin = ['Minimum = ',num2str(min(vec))];
    strmax = ['Maximum = ',num2str(max(vec))];
    figure(i)
    plot(vec, 'bl*')
    %text(indexmin,min(vec),strmin,'HorizontalAlignment','left');
    %text(indexmax,max(vec),strmax,'HorizontalAlignment','left');
    title(strcat(names{i}, ' of Waist: variation based on igm'))
end