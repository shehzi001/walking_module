/**
 * @file
 * @author Alexander Sherikov
 */

#include <iostream>

#include <fstream>
#include <cstdio>
#include <limits>
#include <cmath> // abs, M_PI
#include <cstring> //strcmp

#include "WMG.h"
#include "smpc_solver.h"

#include "zeno_igm.h"
#include "leg2joints.h"
#include "joints_sensors_id.h"


using namespace std;

#include "tests_common.cpp"
#include "draw_SDL.cpp"
#include "init_steps_zeno.cpp"


int main(int argc, char **argv)
{
    //-----------------------------------------------------------
    // sampling
    int control_sampling_time_ms = 40;
    int preview_sampling_time_ms = 80;
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    // initialize classes
    init_waist tdata("test_torso", preview_sampling_time_ms, false);

    
    vector<double> x_coord;
    vector<double> y_coord;
    vector<double> angle_rot;
    tdata.wmg->getFootsteps(x_coord, y_coord, angle_rot);

    /*
    smpc::solver_as solver(
        tdata.wmg->N, // size of the preview window
        4000.0,  // Beta
        1.0,  // Alpha
        0.02,   // regularization
        1.0,    // Gamma
        1e-7);  // tolerance
    */

    double mpc_gain_position = 50.0;   // closeness to the reference ZMP points 
    double mpc_gain_acceleration = 0.02; // penalty for the acceleration
    double mpc_gain_velocity = 1.0;      // penalty for the velocity
    double mpc_gain_jerk = 1.0;          // penalty for the jerk

    double mpc_as_tolerance = 1e-7;
    double mpc_as_max_activate = 5;
    double mpc_as_use_downdate = true;

    smpc::solver_as solver(
                tdata.wmg->N,
                mpc_gain_position,
                mpc_gain_velocity,
                mpc_gain_acceleration,
                mpc_gain_jerk,
                mpc_as_tolerance,
                mpc_as_max_activate,
                mpc_as_use_downdate,
                false); // objective
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    tdata.zeno.getWaistCoM(tdata.zeno.state_sensor, tdata.zeno.CoM_position);
    tdata.par->init_state.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    tdata.X_tilde.set (tdata.zeno.CoM_position[0], tdata.zeno.CoM_position[1]);
    //-----------------------------------------------------------

    std::cout << "Waist pos : [" << tdata.zeno.CoM_position[0] << "," << tdata.zeno.CoM_position[1] << "," << tdata.zeno.CoM_position[2] << "]"<< std::endl;

    initSDL();
    //-----------------------------------------------------------
    // output
    test_log log(tdata.fs_out_filename.c_str());
    //-----------------------------------------------------------

    isRunning=1;
    tdata.wmg->T_ms[0] = control_sampling_time_ms;
    tdata.wmg->T_ms[1] = control_sampling_time_ms;
    while (isRunning)
    {
        tdata.zeno.state_sensor = tdata.zeno.state_model;

        if (tdata.wmg->formPreviewWindow(*tdata.par) == WMG_HALT)
        {
            cout << "EXIT (halt = 1)" << endl;
            break;
        }
        for (unsigned int j = 0; j < tdata.wmg->N; j++)
        {
            log.addZMPrefPoint(tdata.par->zref_x[j], tdata.par->zref_y[j]);
        }

        if (tdata.wmg->isSupportSwitchNeeded())
        {
            Transform<double,3> swing_foot_posture;
            tdata.zeno.getSwingFootPosture (tdata.zeno.state_sensor, swing_foot_posture.data());
            tdata.wmg->changeNextSSPosition (swing_foot_posture.data(), true);

            Transform<double,3> foot_posture;
            tdata.zeno.getPosture(tdata.zeno.state_sensor, foot_posture.data());

            Matrix4d posture = Matrix4d::Map(foot_posture.data());
            //Matrix3d rotation = posture.corner(TopLeft,3,3);
            //angle = rotation.eulerAngles(0,1,2)[2];

            //cout << "waist_posture : " <<  posture << endl;

            tdata.zeno.switchSupportFoot();
            log.addCoMSupportChange(tdata.par->init_state.x(), tdata.par->init_state.y());
        }


        //------------------------------------------------------
        solver.set_parameters (tdata.par->T, tdata.par->h, tdata.par->h[0], tdata.par->angle, tdata.par->zref_x, tdata.par->zref_y, tdata.par->lb, tdata.par->ub);
        solver.form_init_fp (tdata.par->fp_x, tdata.par->fp_y, tdata.par->init_state, tdata.par->X);
        solver.solve();
        //-----------------------------------------------------------
        // update state
        solver.get_next_state(tdata.par->init_state); //com
        solver.get_next_state(tdata.X_tilde);  //zmp
        //-----------------------------------------------------------

        //-----------------------------------------------------------
        // position of CoM
        tdata.zeno.setCoM(tdata.par->init_state.x(), tdata.par->init_state.y(), tdata.par->hCoM);

        // support foot and swing foot position/orientation
        tdata.wmg->getFeetPositions (
                control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWaist(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            //break;
        }
        int failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            //break;
        }  else {
                log.addCoMpoint (tdata.par->init_state.x(), tdata.par->init_state.y());
                log.addZMPpoint (tdata.X_tilde.x(), tdata.X_tilde.y());
                log.addFeetPositions (tdata.zeno);

                /*std::cout << std::endl;
                for(int i=0; i<12;i++) {
                    cout << tdata.zeno.state_model.q[i] << ", ";
                }

                std::cout << std::endl;*/
        }
        //-----------------------------------------------------------


        
        //-----------------------------------------------------------
        // position of CoM
        smpc::state_com next_CoM;
        smpc::state_zmp next_ZMP;
        solver.get_state (next_CoM, 1);
        solver.get_state (next_ZMP, 1);

        tdata.zeno.setCoM(next_CoM.x(), next_CoM.y(), tdata.par->hCoM);

        tdata.wmg->getFeetPositions (
                2*control_sampling_time_ms,
                tdata.zeno.left_foot_posture.data(),
                tdata.zeno.right_foot_posture.data());

        if (tdata.zeno.igmWaist(tdata.ref_angles, 1.2, 0.088, 100) < 0)
        {
            cout << "IGM failed!" << endl;
            //break;
        }
        failed_joint = tdata.zeno.state_model.checkJointBounds();
        if (failed_joint >= 0)
        {
            cout << "MAX or MIN joint limit is violated! Number of the joint: " << failed_joint << endl;
            //break;
        } else {
                log.addCoMpoint (next_CoM.x(), next_CoM.y());
                log.addFeetPositions (tdata.zeno);
                log.addZMPpoint (next_ZMP.x(), next_ZMP.y());
        }

        //-----------------------------------------------------------
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {

            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }
        usleep (100000);
    }

    // keep the visualization active (until ESC is pressed)
    while (isRunning)
    {
        if (tdata.zeno.support_foot == IGM_SUPPORT_RIGHT)
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.right_foot_posture);
        }
        else
        {
            drawSDL(0, x_coord, y_coord, angle_rot, tdata.zeno.support_foot, tdata.zeno.state_model.q, tdata.zeno.left_foot_posture);
        }
    }

    //-----------------------------------------------------------
    // output
    log.flushLeftFoot ();
    log.flushRightFoot ();
    log.flushZMP ();
    log.flushZMPref ();
    log.flushCoM ();
    log.flushCoMSupportChange();
    //-----------------------------------------------------------

    return 0;
}
